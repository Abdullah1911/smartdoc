1. OTHER PROPAGANDA THEORISTS

1 Other Propaganda Theorists

1.1 Harold Lasswell (1902-1978)

As Lippmann was writin
fact, much of the propa
(Rogers, 1994).

Harold Lasswell (1902
. -1978) was a romine t t
conductmg both quantitative and qualitatliive analn scholar in the area of propaganda research. He focused on
and discovering the effect of propaganda on th yses of propaganda, understanding the content of propaganda,
the mass communication procedure of e mass audience (Rogers, 1994). Lasswell is credited with creating
defined as, umthe investigation of commliiltjinterlt analysrs (Rogers, 1994). Generally, content analysis can be
in order to measure certain variables.) R canon messages by categorizing message content into classifications
Lasswell (1946) 1 - ( ogers, 1994). In an essay entitled llContents of Communication ll
SymbOIS appear iexp ams that 31 content analysis should take into account the frequency with which certain
n a message, e direction in which the s mbol . i . l
the intensity of the SymbOIS used B . y 3 try to persuade the audience 5 opunon, and
I . y understanding the content of the messa e L 11 .
achieve the cal of unde t i ll 1 g , asswe (1946) ms to
to andiencengm 74). rs andmg the stream of influence that runs from control to content and from content
This method of content analysis is tied strongly to Lasswell (1953) early definition of communication which
stated, Who says what m which channel to whom and with what effectsll (p. 84). Content analysis was essen-
tially the says what part of this definition, and Lasswell went on to do a lot of work within this area during the
remainder of his career.
Aside from understanding the content of propaganda, Lasswell was also interested in how propaganda could

shape public opinion. This dealt primarily with understanding the effects of the media. Lasswell was particularly
e media in creating public opinion within a democratic system. In this
public is limited in the information that is presented to them,
till that information that is affecting their decisions within
government. This is an interesting way of viewing

ro
gaiidapglgaanldaagsHarlclild Lasswell was undertaking empirical analyses of propaganda In
we was examlmng was actually being written by Lippmann himself

interested in examining the effects of th
way, Lasswell has created a cycle, whereby the
and also apprehensive to accept it. However, it is s

the democratic system, and is being presented to them by the
the power of the media that is somewhat similar to Lippmann,s theories.

1.2 Edward Bernays (1891-1995)

Lasswell were examining public opinion and propaganda,
elations, propaganda, and public opinion. Bemays (1928)
create or shape events to influence the relations of a
r propaganda theorists, Bernays recognizes
It can help individuals decide what to think
ficial to societyis functioning as a whole.
gested, largely by
if they are to live

At approximately the same time that Lippmann and

Edward Bernays (1891-1995) was examining public r

defines propaganda as, a consistent, enduring effort to
ise, idea, or group (p. 25). Contrary to othe

public to an enterpri p
that propaganda can be either beneficial or harmful to the pubhc.
about or alter the opinions of individuals, but this may actually be bene .
Bemays states, ffWe are governed, our minds are molded, our tastes formed, .our ideas sug
men we have never heard of... Vast numbers of human beings must cooperate 1n thls manner
togaher as a smOOthly funcuonmg socmty (p. 9). dified, and that such shaping is a necessary part of

e ideas that the public opinion can be mo - I . I I

tBased on thesursued his work in the field of public relations. liPubhc relatlons Is the attempt, by information,
SOCIEtyiBemays P gineer public support for an activity, cause, movement, or institution (Bemays,
persuasmn, derstanding the public

and adjustment, to en k for W

i l C nt, Bemays (1955) lays out the framewor . I
1955, p. 3). ln the Engmeermg Of GOHZISJZIQH. Bernays (1955) claims that the key to a successful pubhc relations
and developing a athering information

public relations C _ . S in society g
. , . n to the attitudes of various group , i , . . .
calIIlPaign 15 adlusmlerlt 0f the camlfrlalgly utilizing persuasion to influence the PUbhc 0Pm10n m the Intended
to effectiVEIY expr ,

ess an idea, and f
direction.
Bernaysi theories represent a. step
typical presentations of fthlt-or-miss pro

mmunication theory. They move away from more

forward for mass C0
toward a deeper

Pagandaf, and move

 

