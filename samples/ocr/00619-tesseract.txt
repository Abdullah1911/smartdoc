1. DEFINING UNCERWNTY

Research conducted under the title of interpersonal communication initially focused on persuasion

ngsmgzlencg, and small group processes. Theories explored the role of learning, dissonance, bal-

, Ju gment, and reactance (Berger, 2005). Kurt Lewin, a forefather of social psychology
played a consrderable role in influencing interpersonal research pioneers such as Festinger, Heider,
Kelley, and Hovland. By the 19705, research interests began to shift into the realm of social interaction,
relational development, and relational control. This was influenced by the research of such scholars
as Knapp, Mehrabian, Altman, Taylor, Duck, Kelley, and Thihaut. During the later part of the decade
and into the 19805, the cognitive approaches of Hewes, Planalp, Roloff, and Berger became popular
along with research into behavioral and communicative adaptation by Giles, Burgoon, and Patterson.
Berger (2005) states: uthese early theoretical forays helped shape the interpersonal comm research
agenda during the past two decadesu (p. 416). Today, interpersonal communication tends to focus
on dyadic communication, communication involving face-to-face interaction, or communication as
a function of developing relationships. Research into interpersonal communication theory typically
focuses on the development, maintenance, and dissolution of relationships. It has been recognized
that interpersonal communication is motivated by uncertainty reduction (Berger 8: Calabrese, 1975).
Since its introduction in the 19705, uncertainty has been recognized as a major field of study that has
contributed to the development of the field of communication as a whole. This chapter strives to focus
on those theorists who pioneered the research of uncertainty reduction in communication. Their work
is crucial to the development of the field of interpersonal communication, and is central in our
understanding of interpersonal processes.

1 Defining Uncertainty

Since uncertainty has been identified as an important construct, necessary to the study of commu-
nication, it would be beneficial to know when the concept originated, and how it has been defined
and studied. One way to consider uncertainty is through the theoretical framework of information
theory. Shaman and Weaver (1949) proposed that uncertainty existed in a given situation when there
was a high amount of possible alternatives and the probability of their event was relatively equal.
Shannon and Weaver related this view of uncertainty to the transmission of messages, but their work
also contributed to the development of URT. Berger and Calabrese (1975) adopted concepts from the
information theorists as well as Heider (1958) research in attribution. Berger attdCalabrese (1975)
expanded the concept of uncertainty to fit interpersonal communicationby defining uncertainty as
the unumber of alternative ways in which each interactant might behave i .(p. 100). The greater the
level of uncertainty that exists in a situation, the smaller the chance indiViduals wrllbe able to pre-
dict behaviors and occurrences. During interactions indivrduals are not only faced with problems of
predicting present and past behaviors, but also explaining why partners behave or believle thefvg:
that they do. Berger and Bradacls (1982) definition of uncertainty highlighted thefcslmp exity thin
process when they stated: trUncertainty, then, can stem from the large number a tamative . I gs
that a stranger can believe or potentially sayil Uncertainty plays a stgmficant role w en examining

relationships High levels of uncertainty can severely inhibit relational development. Uncertainty can

cause stress and anxiety which can lead to low levels of communicator competence (West & Turner,

1 not be able to develop relationships or may be too anxious
2000). InconFtizllirithEledlrlglafsinaagd Turner (2000) note that lower levels of uncertainty caused
m engag: in 111212111 and nonverbal behavior, increased levels of intimacy, and increased liking. In inter
increase vilid is are expected to increase predictability with the goal that this Will lead to the ability
anions mm M i in what will occur in future interactions. When high uncertainty exists it is often
to pmdm and ehpthais oal Although individuals seek to reduce uncertainty, high levels of certainty
idiggltgtggillty ctlin galsolinliihlt a relationship. Heath and Bryant (2000) state: mroo much certainty

and predictability can deaden a relationship:

 

