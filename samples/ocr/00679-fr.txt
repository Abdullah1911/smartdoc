﻿1
OTHER PROPAGANDA THEORISTS
|Bther Propaganda Theorists
1-1 Harold Lasswell (1902-1978)
fact, much of the pTlplgandaThat^Ussw 11^ LaSSWeU Was Undertakin8 empirical analyses of propaganda I (Rogers, 1994).	? §	** LaSSWeU W3S examinin§ was actually being written by Lippmann himsel
conducting both^uandtTtive1 and^ualt	SCh°lar in 016 area of Propaganda research. He focused oi
and discovering the effect of nrorJL H analyses of Propaganda, understanding the content of propaganda the mass communication nrored 8 a °n 6 mass audience (Rogers, 1994). Lasswell is credited with creating defined as	T 1	(Ro«^ 1994). Generally, content analysis can b!
in order to mpa	83 100 ° communication messages by categorizing message content into classification'
VariableS" (R°8-s> 1994). In an essay entitled "Contents of Communication/ Lasswell (1946) explains that a content analysis should take into account the frequency with which certain
s appear in a message, the direction in which the symbols try to persuade the audience's opinion, and
me interval tv nr ttr o cTimKni n	_i__._t _ . i	„
i •	-	^	o	v-iil ui Liic	i-»dboWell ^JLC/^fUy dims 1C
achieve the goal of understanding the "stream of influence that runs from control to content and from conteni to audience" (p. 74).
This method of content analysis is tied strongly to Lasswell (1953) early definition of communication which stated, Who says what in which channel to whom and with what effects" (p. 84). Content analysis was essentially the says what part of this definition, and Lasswell went on to do a lot of work within this area during the remainder of his career.
Aside from understanding the content of propaganda, Lasswell was also interested in how propaganda could shape public opinion. This dealt primarily with understanding the effects of the media. Lasswell was particularly interested in examining the effects of the media in creating public opinion within a democratic system. In this way, Lasswell has created a cycle, whereby the public is limited in the information that is presented to them, and also apprehensive to accept it. However, it is still that information that is affecting their decisions within the democratic system, and is being presented to them by the government. This is an interesting way of viewing the power of the media that is somewhat similar to Lippmann’s theories.
1.2 Edward Bernays (1891-1995)
At approximately the same time that Lippmann and Lasswell were examining public opinion ana propaganaa, Edward Bernays (1891-1995) was examining public relations, propaganda, and public opinion Bernays (	)
defines propaganda as, a consistent, enduring effort to create or shape events to influence the relations of a
S ™rr S Si benefill o, ££!!,» nj£p ^ *»■ wha, .0
at,™ or ate‘	minds « m(,ldedi our tastes formed, our ,de,s suggested, large., by
me'oSe have never heard of.,. Vast numbers of human beings mus. ccmperate in this manner if die, are
together as a smoothly functioning society (p. 9).	dified and that such shaping is a necessaiy part of
Based on these ideas that the pu c °P™	relations "Public relations is the attempt, by information,
society, Bernays pursued his work	"ivity, cause, movement, or institution" (Bernays,
and	developing a public relations campaign	various groups in society, gathering information
campaigners	itiliang persuasion to influence die public opinion in the intended
dhecdonVely P	st fomard for mass communicadon theory. The, move awa, from mote
Bernays’ theories repre	P ganda>» and move toward a deeper
typical presentations of hit or m y