/** 
 * @file opencv-tess-utils.h 
 * This file contains various utility functions for interfacing Tesseract 
 * and OpenCV and performing some basic image processing functions. 
 * 
 * @author Faisal Shafait (faisalshafait@gmail.com)  
 * 
 * @version 0.1  
 * 
 */ 
 
#ifndef OPENCV_TESS_UTILS_H 
#define OPENCV_TESS_UTILS_H 
 
#include "stdio.h" 
#include <string> 
#include <fstream> 
#include <vector> 
#include <assert.h>
#include "union-find.h"
// Tesseract headers 
#include <tesseract/baseapi.h> 
#include <leptonica/allheaders.h> 
// Open CV headers 
#include "opencv2/core/core.hpp" 
#include "opencv2/highgui/highgui.hpp" 
#include "opencv2/imgproc/imgproc.hpp" 
 
Pix * MAT2PIXBinary(cv::Mat &imgMAT);

Pix * MAT2PIXGray(cv::Mat &imgMAT);

Pix * MAT2PIXRGB(cv::Mat &imgMAT);

bool PIX2MATGray(Pix *imgPIX, cv::Mat &imgMAT);

bool PIX2MATRGB(Pix *imgPIX, cv::Mat &imgMAT);

void findConComp(cv::Mat &img, std::vector<cv::Rect> &rboxes,
                 float max_x, float max_y, float min_area, int type);

unsigned int levenshtein_distance(std::string &s1, std::string &s2);

#endif 
  
