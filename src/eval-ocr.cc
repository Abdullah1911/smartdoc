#include "opencv-tess-utils.h"
#include <iostream>

using namespace std;
using namespace tesseract;
using namespace cv;

int main(int argc, char **argv){
    if(argc < 3){
        cerr << "Usage: " << argv[0] << " <gt.txt> <ocr.txt>\n";
        exit(0);
    }
    // Test cases
    string str1, str2;
    str1 = "ISLANDER";  str2 = "SLANDER";
    assert(levenshtein_distance(str1, str2) == 1);
    str1 = "MART";  str2 = "KARMA";
    assert(levenshtein_distance(str1, str2) == 3);
    str1 = "KITTEN";  str2 = "SITTING";
    assert(levenshtein_distance(str1, str2) == 3);
    str1 = "INTENTION";  str2 = "EXECUTION";
    assert(levenshtein_distance(str1, str2) == 5);
    
    // Actual computation
    string src(argv[1]);
    string target(argv[2]);
    int dist = levenshtein_distance(src, target);
    cout << "Edit Distance = " << dist << endl;
    return 0;
}
