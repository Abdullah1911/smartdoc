#include "smartDoc.h"

using namespace std;
using namespace tesseract;
using namespace cv;
using namespace pugi;
using namespace boost::filesystem;

string DEBUGFILESDIRECTORY = "/home/shoaib/smartdoc/debug";
string OUTPUTXMLFILEDIRECTORY = "/home/shoaib/smartdoc/XMLFiles/";
string TEXTFILESDIRECTORY = "/home/shoaib/smartdoc/output/";
vector<Rect> DocRectangle;

int lastFrameIndexStoredInXML;


#define CHALLENGE 1			//Challenge 1 = Document Detection; Challenge 2 = Document OCR


class Boundingbox{
public:
Point topLeftPt;
Point topRightPt;
Point BottomLeftPt;
Point BottomRightPt; 
Rect ImageRect;
};

vector<Boundingbox> boxCorners;



Mat Getcontours(Mat cannyImage)
{
	//Boundary Detection using Countour Finding Method
	vector< vector<Point> > contoursCanny, contoursBinary;
	vector<Vec4i> hierarchy;
	findContours( cannyImage, contoursCanny, hierarchy, RETR_EXTERNAL,  CHAIN_APPROX_SIMPLE );
	//findContours( imgMATOrig, contoursBinary, hierarchy, RETR_EXTERNAL,  CHAIN_APPROX_SIMPLE );

	/// Approximate contours to polygons and get bounding rects
	vector< vector<Point> > contours_poly( contoursCanny.size() );
	vector<Rect> boundRect( contoursCanny.size() );
	vector< vector<Point> > contoursBin_poly( contoursBinary.size() );
	vector<Rect> boundRectBin( contoursBinary.size() );

	Rect maxAreaRect;
    double maxArea = 0;
    double area = 0;

  
    //Compute the area of the contours and keep track of the rect with the biggest area
	for( int i = 0; i < contoursCanny.size(); i++ )
	{ 
		approxPolyDP( Mat(contoursCanny[i]), contours_poly[i], 3, true );
		boundRect[i] = boundingRect( Mat(contours_poly[i]) );

		area = contourArea(contoursCanny[i]);
        if( area > maxArea )
        {
            maxArea = area;
            maxAreaRect = boundRect[i];
        }
	}

	for( int i = 0; i < contoursBinary.size(); i++ )
	{ 
		approxPolyDP( Mat(contoursBinary[i]), contoursBin_poly[i], 3, true );
		boundRectBin[i] = boundingRect( Mat(contoursBin_poly[i]) );

		area = contourArea(contoursBinary[i]);
        if( area > maxArea )
        {
            maxArea = area;
            maxAreaRect = boundRectBin[i];
        }
	}

	printf("Canny contours: %d, Binary contours: %d\n", int(contoursCanny.size()), int(contoursBinary.size()));

	/// Draw polygonal contour + bonding rects
	Mat drawing = Mat::zeros( cannyImage.size(), CV_8UC3 );
	for( int i = 0; i< contoursCanny.size(); i++ )
	{
		Scalar color = Scalar(255,0,255);
		drawContours( drawing, contours_poly, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
		rectangle( drawing, boundRect[i].tl(), boundRect[i].br(), color, 2, 8, 0 );          
	}
	//imshow("Contour Detector", drawing);
	return drawing;
}


Mat Probabilistic_houghLines(Mat imgMATOrig, int threshold, int minLineLength, int maxLineGap){
	//Boundary Detection using Probabilistic Hough Lines
	

	/*int minLineLength = 10;
	int maxLineGap = 100;
	int threshold = 5;*/	
		
	//Number of intersections to detect a line
	
	vector<Vec4i> lines;
	HoughLinesP(imgMATOrig, lines, 1, CV_PI/180, threshold, minLineLength, maxLineGap );
	Mat pImg = Mat::zeros(imgMATOrig.rows, imgMATOrig.cols, CV_8UC1);
	for( size_t i = 0; i < lines.size(); i++ )
	{
		Vec4i l = lines[i];
		line( pImg, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255), 20, LINE_AA);
	}
	//imshow("Probabilistic_houghLines canny", imgMATOrig);
	pImg = 255 - pImg;
    //imshow("Probabilistic_houghLines ", pImg);
	return pImg;
}

Mat Std_houghLines(Mat houghLinesMat,int threshold){

	//Boundary Detection using Standard Hough Lines
	//int threshold = 5;
	
	vector<Vec2f> lines;
	HoughLines(houghLinesMat, lines, 1, CV_PI/180, threshold, 0, 0 );

	for( size_t i = 0; i < lines.size(); i++ )
	{
		float rho = lines[i][0], theta = lines[i][1];

		float thetaDeg = theta * 180;
		//printf("Theta %f and Theta(Deg) %f", theta, thetaDeg );

		//Check if the line is vertical or horizontal
		if( ((thetaDeg >= 0) && (thetaDeg < 10)) || ((thetaDeg > 170) && (thetaDeg < 200)) )
		{
			//printf("Range detected");
			Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a*rho, y0 = b*rho;
		pt1.x = cvRound(x0 + 1000*(-b));
		pt1.y = cvRound(y0 + 1000*(a));
		pt2.x = cvRound(x0 - 1000*(-b));
		pt2.y = cvRound(y0 - 1000*(a));
		line( houghLinesMat, pt1, pt2, Scalar(255,0,0), 3, LINE_AA);
		}

		Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a*rho, y0 = b*rho;
		pt1.x = cvRound(x0 + 1000*(-b));
		pt1.y = cvRound(y0 + 1000*(a));
		pt2.x = cvRound(x0 - 1000*(-b));
		pt2.y = cvRound(y0 - 1000*(a));
		line( houghLinesMat, pt1, pt2, Scalar(255,0,0), 3, LINE_AA);
	}

	//imshow("Standard Hough Lines", houghLinesMat);
    return houghLinesMat;
}



Mat GetCannyImage(Mat grayscaleImg,int lowThreshold,int ratio,int kernel_size)
{
	/// Reduce noise with a kernel 3x3
	blur( grayscaleImg, grayscaleImg, Size(15,15) );

/*	int lowThreshold = 75, ratio = 3;
	int kernel_size = 5;*/
	Mat cannyImage;

	/// Canny detector
	Canny( grayscaleImg, cannyImage, lowThreshold, lowThreshold*ratio, kernel_size );
	//imshow("cannyimg Frame", cannyImage);
	return cannyImage;
}

Mat lineSegmentDectector(Mat cannyImage)
{
	//Apply OpenCV 3 LineSegmentDetector
    Ptr<LineSegmentDetector> ls = createLineSegmentDetector(LSD_REFINE_STD);
    
    vector<Vec4i> lines_std;

    // Detect the lines
    ls->detect(cannyImage, lines_std);

    // Show found lines
    Mat drawnLines(cannyImage);
    ls->drawSegments(drawnLines, lines_std);

    //imshow("LSD Output", drawnLines);
    return drawnLines;
}

//#define VIDEOFILESDIRECTORY "/home/shoaib/SmartDoc/smartdoc/samples"
//#define IMAGEFILESDIRECTORY "/home/shoaib/SmartDoc/smartdoc/samples"

/** 
 * This function is responsible for reading a video file
 * and detecting documents in each of the frames by delegating this task to Detect function
 */ 
void SmartDoc::readVideoFiles(string path)
{
 	vector<string> files;
 	getFilesFromDirectory(path, files, VIDEOFILESFORMAT);

 	if(files.size() == 0)
 	{
 		cerr << "Error: Unable to find any files" << endl;
 		exit(-1);
 	}

	//Iterate over all the files
	for(int i = 0; i < files.size(); i++)
	{
		cout << "Starting video: " << files[i] << endl;

		//Open the video file for reading
		VideoCapture videoCapture(files[i]);

		//Open the video file for reading
		VideoCapture videoCapture1(files[i]);

        DocRectangle.clear();
        lastFrameIndexStoredInXML = 0;

		if(!videoCapture.isOpened())
		{
			cerr << "Error: Unable to open video file" << endl;
			exit(-1);
		}

		//backgroundSubtraction(videoCapture);
		//return;
        
        //Extract the video file name from the absolute path
        videoFileName = files[i].substr(files[i].find_last_of("/") + 1);
       
        string temp = files[i].substr(0, files[i].find_last_of("/"));
        temp = temp.substr(temp.find_last_of("/") + 1);
        //cout << temp << endl;
        videoFileName = videoFileName.substr(0, videoFileName.find_last_of("."));
       
       
		//Create basic structure in the file
		initializeXMLFile(temp);

        videoFileName += ".segresult.xml";
        videoFileName = OUTPUTXMLFILEDIRECTORY + videoFileName;
        cout << videoFileName << endl;

		//Start reading the video frames
		int frameCount = videoCapture.get(CAP_PROP_FRAME_COUNT);
		cout << "Frame Count: " << frameCount << endl;
		Mat frame;

		//Iterate over all the frames in the video
		for(int frameNo = 0; frameNo < frameCount; frameNo++)
		{
		    //if (frameNo > 51) break;
			//Capture frame
			videoCapture >> frame;

			if(frame.empty())
			{
				cerr << "Error: Empty frame found" << endl;
				break;
			}

            //if(frameNo%3) continue;
			//Send the frame for detection
			Mat processedFrame;
            basicDetection(frame);
			//processFrame(frame, processedFrame, (frameNo + 1));
		}

        //Validate the XML created
        //validateXML(frameCount);

/*
        for(int FrameNo=0;FrameNo<frameCount;FrameNo++){
        	videoCapture1 >> frame;
            Mat croppedframe=frame(boxCorners[FrameNo].ImageRect);
     		fixedCordinates(frame,FrameNo);

        }*/

		//Write the XML document to file
		document.print(cout);
    	document.save_file(videoFileName.c_str());
       
	}
}

void SmartDoc::basicDetection(Mat& mat)
{
    cv::cvtColor(mat, mat, CV_BGR2GRAY);
    cv::GaussianBlur(mat, mat, cv::Size(3,3), 0);
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Point(9,9));
    cv::Mat dilated;
    cv::dilate(mat, dilated, kernel);

    cv::Mat edges;
    cv::Canny(dilated, edges, 84, 3);

    std::vector<cv::Vec4i> lines;
    lines.clear();
    cv::HoughLinesP(edges, lines, 1, CV_PI/180, 25);
    std::vector<cv::Vec4i>::iterator it = lines.begin();
    for(; it!=lines.end(); ++it) {
        cv::Vec4i l = *it;
        cv::line(edges, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(255,0,0), 2, 8);
    }
    std::vector< std::vector<cv::Point> > contours;
    cv::findContours(edges, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_KCOS);
    std::vector< std::vector<cv::Point> > contoursCleaned;
    for (int i=0; i < contours.size(); i++) {
        if (cv::arcLength(contours[i], false) > 100)
            contoursCleaned.push_back(contours[i]);
    }
    std::vector<std::vector<cv::Point> > contoursArea;

    for (int i=0; i < contoursCleaned.size(); i++) {
        if (cv::contourArea(contoursCleaned[i]) > 10000){
            contoursArea.push_back(contoursCleaned[i]);
        }
    }
    std::vector<std::vector<cv::Point> > contoursDraw (contoursCleaned.size());
    for (int i=0; i < contoursArea.size(); i++){
        cv::approxPolyDP(Mat(contoursArea[i]), contoursDraw[i], 40, true);
    }
    //Mat drawing = Mat::zeros( mat.size(), CV_8UC3 );
    Mat drawing = mat.clone();
    cv::drawContours(drawing, contoursDraw, -1, cv::Scalar(0,255,0),1);

    imshow("Final", drawing);
    waitKey(50);
}


Rect getBounds(Mat imgBin)
{
	int documentLeft, documentTop, documentRight, documentBottom; 
    double borderThresh = 0.8; 
    for(int i=0; i<imgBin.rows; i ++){ 
        double blackPixels = 0; 
        for(int j=0; j<imgBin.cols; j++){ 
            if (imgBin.at<uchar>(i,j) == 0) 
                blackPixels ++; 
        } 
        if( (blackPixels / imgBin.cols) < borderThresh){ // top edge found 
            documentTop = i; 
            break; 
        } 
    } 
 
    for(int i=imgBin.rows-1; i>=0; i--){ 
        double blackPixels = 0; 
        for(int j=0; j<imgBin.cols; j++){ 
            if (imgBin.at<uchar>(i,j) == 0) 
                blackPixels ++; 
        } 
        if( (blackPixels / imgBin.cols) < borderThresh){ // top edge found 
            documentBottom = i; 
            break; 
        } 
    } 
 
    for(int j=0; j<imgBin.cols; j++){ 
        double blackPixels = 0; 
        for(int i=0; i<imgBin.rows; i ++){ 
            if (imgBin.at<uchar>(i,j) == 0) 
                blackPixels ++; 
        } 
        if( (blackPixels / imgBin.rows) < borderThresh){ // top edge found 
            documentLeft = j; 
            break; 
        } 
    } 
 
    for(int j=imgBin.cols-1; j>=0; j--)
    { 
        double blackPixels = 0; 
        for(int i=0; i<imgBin.rows; i ++)
        { 
            if (imgBin.at<uchar>(i,j) == 0) 
                blackPixels ++; 
        } 
        if( (blackPixels / imgBin.rows) < borderThresh){ // top edge found 
            documentRight = j; 
            break; 
        } 
    }
	Rect document(documentLeft, documentTop, documentRight - documentLeft, documentBottom - documentTop); 
	return document;
}


int SmartDoc::GetChannel(Mat &frame, cv::Rect doc){


vector<double> mean1=getMeanPixelValuesInnerRect(frame,doc);
vector<double> mean2=getMeanPixcelValuesOuterRect(frame,doc);

double DiffBlue= std::abs(mean1[0]-mean2[0]);
double DiffGreen= std::abs(mean1[1]-mean2[1]);
double DiffRed= std::abs(mean1[2]-mean2[2]);


Mat channel[3];
split(frame, channel);

if(DiffBlue>DiffRed && DiffBlue>DiffGreen ){
return 0;
}

else if(DiffGreen>DiffBlue && DiffGreen>DiffRed){
return 1;
}

else if(DiffRed>=DiffBlue && DiffRed>=DiffGreen){
return 2;
}

return 0;
}


/*Mat erode;
Mat element = getStructuringElement(cv::MORPH_CROSS,
              cv::Size(9,9),
              cv::Point(9, 9));
 
//cv::erode(houghlinesImg,erode,element); 

*/
void SmartDoc::backgroundSubtraction(VideoCapture cap)
{
	string method = "knn";
	bool smoothMask = true;
	bool update_bg_model = true;

    Ptr<BackgroundSubtractor> bg_model = method == "knn" ?
        createBackgroundSubtractorKNN().dynamicCast<BackgroundSubtractor>() :
        createBackgroundSubtractorMOG2().dynamicCast<BackgroundSubtractor>();

    Mat img0, img, fgmask, fgimg;

    for(;;)
    {
        cap >> img0;

        if( img0.empty() )
            break;

        resize(img0, img, Size(640, 640*img0.rows/img0.cols), INTER_LINEAR);

        if( fgimg.empty() )
          fgimg.create(img.size(), img.type());

        //update the model
        bg_model->apply(img, fgmask, update_bg_model ? -1 : 0);
        if( smoothMask )
        {
            GaussianBlur(fgmask, fgmask, Size(11, 11), 3.5, 3.5);
            threshold(fgmask, fgmask, 10, 255, THRESH_BINARY);
        }

        fgimg = Scalar::all(0);
        img.copyTo(fgimg, fgmask);

        Mat bgimg;
        bg_model->getBackgroundImage(bgimg);

       // imshow("image", img);
        //imshow("foreground mask", fgmask);
        //imshow("foreground image", fgimg);
        if(!bgimg.empty())
          imshow("mean background image", bgimg );

        char k = (char)waitKey(30);
        if( k == 27 ) 
        	break;
        if( k == ' ' )
        {
            update_bg_model = !update_bg_model;
            if(update_bg_model)
                printf("Background update is on\n");
            else
                printf("Background update is off\n");
        }
    }

    return;
}

void connectedComponentAnalysis(Mat& img, Mat& labeledImage)
{
    Mat labelImage(img.size(), CV_32S);
    int nLabels = connectedComponents(img, labelImage, 8);

    std::vector<Vec3b> colors(nLabels);
    colors[0] = Vec3b(0, 0, 0);//background
    for(int label = 1; label < nLabels; ++label)
    {
        colors[label] = Vec3b( (rand()&255), (rand()&255), (rand()&255) );
    }

    Mat dst(img.size(), CV_8UC3);
    for(int r = 0; r < dst.rows; ++r)
    {
        for(int c = 0; c < dst.cols; ++c)
        {
            int label = labelImage.at<int>(r, c);
            Vec3b &pixel = dst.at<Vec3b>(r, c);
            pixel = colors[label];
         }
     }

    //imshow( "Connected Components", dst );
    //waitKey(0);

    labeledImage = dst;
}

std::vector<double> SmartDoc::getMeanPixcelValuesOuterRect(Mat &frame, cv::Rect doc){

   int offset=50;
try{
// Adding offset to get a bigger rectangle
Rect doc2=Rect(doc.x-offset,doc.y-offset,doc.width+offset*2, doc.height+offset*2); 

vector<cv::Vec3b> pixelValue;

Rect ImageBounds=Rect(0,0,frame.cols-1,frame.rows-1);
Rect IntersetedRect;
IntersetedRect = ImageBounds & doc2;

//retriving x2 , y2 of rectangle 
int x2=IntersetedRect.x+IntersetedRect.width;
int y2=IntersetedRect.y+IntersetedRect.height;


//rectangle(frame, Point(IntersetedRect.x,IntersetedRect.y),Point(x2,y2), Scalar(0,255,255), 3);


//rectangle(frame, doc, Scalar(0,0,255), 3);
//rectangle(frame, doc, Scalar(0,0,255), 3);
//rectangle(frame, IntersetedRect, Scalar(0,0,255), 3);

//imshow("oute rrect",frame);
//waitKey(0);


 // Finding pixels of in Rect doc2 i.e. the bigger rect than a document rectangle, to find the
 // pixels of the image background
    for(int col=IntersetedRect.x; col<x2; col++){
        for(int row=IntersetedRect.y; row<y2; row++){
            if((col> doc.x && col<doc.x+doc.width) && (row>doc.y && row<doc.y+doc.height)){
            }else {
     
                
                Vec3b bgrPixel; 
                bgrPixel[0] = frame.at<cv::Vec3b>(row,col)[0];
                bgrPixel[1] = frame.at<cv::Vec3b>(row,col)[1];
                bgrPixel[2] = frame.at<cv::Vec3b>(row,col)[2];
                
                pixelValue.push_back(bgrPixel);
            
            }
        }

        }
vector<double> meanValuesOfBGR;
//mean value of Blue pixels
        double meanB=0;
    
        for(int i=0;i<pixelValue.size();i++)
        {
            
            meanB=meanB+int(pixelValue[i].val[0]);
        }
        
            meanB=meanB/pixelValue.size();
            cout<<" meanB="<<meanB;
meanValuesOfBGR.push_back(meanB);

//mean value of Green pixels
        double meanG=0;
        for(int i=0;i<pixelValue.size();i++)
        {
            meanG=meanG+int(pixelValue[i].val[1]);
        }
        
            meanG=meanG/pixelValue.size();
           cout<<" meanG="<<meanG;
meanValuesOfBGR.push_back(meanG);

//mean value of Red pixels
        double meanR=0;
        for(int i=0;i<pixelValue.size();i++)
        {
            meanR=meanR+int(pixelValue[i].val[2]);
        }
       
            meanR=meanR/pixelValue.size();
             cout<<" meanR="<<meanR;
meanValuesOfBGR.push_back(meanR);
cout<<endl;
        return meanValuesOfBGR; 

    }catch(exception e){
vector<double> meanValuesOfBGR;
        meanValuesOfBGR.push_back(0);
          meanValuesOfBGR.push_back(0);
            meanValuesOfBGR.push_back(0);
            return meanValuesOfBGR;
}
}

std::vector<double> SmartDoc::getMeanPixelValuesInnerRect(Mat &frame, cv::Rect doc){
int offset1=20;

try{
// Adding offset to get a bigger rectangle
Rect doc1=Rect(doc.x+offset1,doc.y+offset1,doc.width-(offset1*2), doc.height-(offset1*2));     

vector<cv::Vec3b> pixelValue;

cout<<"doc y="<<doc.x<<endl;
cout<<"doc x="<<doc.y<<endl;



//retriving x2 , y2 or rectangle 
int x2=doc1.x+doc1.width;
int y2=doc1.y+doc1.height;

Mat grayscaleImg;
cvtColor(frame, grayscaleImg, COLOR_BGR2GRAY);

Mat BinaryImage; 
threshold(grayscaleImg, BinaryImage, 0, 255, THRESH_BINARY | THRESH_OTSU); 

// Finding pixels of in Rect doc1 i.e. the smaller rect than a document rectangle, to find the
// pixels of the document background

    for(int i=doc1.x;i<=x2;i++){
        for(int j=doc1.y;j<=y2;j++){
    
            //cout<<i<<" "<<j<<endl;
            //BinaryImage.at<uchar>(j,i)=0;
                            
            if(BinaryImage.at<uchar>(j,i)==255){
                Vec3b bgrPixel; 
                bgrPixel[0] = frame.at<cv::Vec3b>(j,i)[0];
                bgrPixel[1] = frame.at<cv::Vec3b>(j,i)[1];
                bgrPixel[2] = frame.at<cv::Vec3b>(j,i)[2];
                
                //cout<<"bi pixls "<<int(BinaryImage.at<uchar>(i,j))<<endl;
                //circle(frame, Point(i,j), 0.1, Scalar(255,0,0));
                pixelValue.push_back(bgrPixel); 
            }   
            //cout<<" j="<<j<<endl;
            
     
        }
    }


/*
rectangle(frame, doc1, Scalar(0,0,255), 3);
//imshow("inner rrect1",frame);
imshow("frame",frame);
BinaryImage.at<uchar>(j,i)=0;
imshow("BinaryImage",BinaryImage);
waitKey(0);*/
vector<double> meanValuesOfBGR;

//mean value of Blue pixels
        double meanB=0;

        for(int i=0;i<pixelValue.size();i++)
        {
            meanB=meanB+int(pixelValue[i].val[0]);
        }
        
            meanB=meanB/pixelValue.size();
        meanValuesOfBGR.push_back(meanB);
        cout<<" Doc meanB="<<meanB;
//mean value of Green pixels
        double meanG=0;
        for(int i=0;i<pixelValue.size();i++)
        {
            meanG=meanG+int(pixelValue[i].val[1]);
        }
       
            meanG=meanG/pixelValue.size();
        cout<<" Doc meanG="<<meanG;     
meanValuesOfBGR.push_back(meanG);
        
//mean value of Red pixels
        double meanR=0;
        for(int i=0;i<pixelValue.size();i++)
        {
            meanR=meanR+int(pixelValue[i].val[2]);
        }
        
        meanR=meanR/pixelValue.size();
        cout<<" Doc meanR="<<meanR;
meanValuesOfBGR.push_back(meanR);
cout<<endl;

return meanValuesOfBGR;
}catch(exception e){
        vector<double> meanValuesOfBGR;
        meanValuesOfBGR.push_back(0);
          meanValuesOfBGR.push_back(0);
            meanValuesOfBGR.push_back(0);
            return meanValuesOfBGR;
}

}





cv::Rect SmartDoc::ProcessImage_MeanValue(Mat &frame, Mat &grayscaleImg){


try{
Mat cannyimg=GetCannyImage(grayscaleImg,75,3,5);
//Mat prob_houghLines=Probabilistic_houghLines(cannyimg,5,100,100); 

Mat dilate;
Mat element = getStructuringElement(cv::MORPH_CROSS,
              cv::Size(17,17),
              cv::Point(9, 9));
 
cv::dilate(cannyimg,dilate,element); 

for(int i=0;i<dilate.rows;i++){
 for(int j=0;j<dilate.cols;j++){
        if(dilate.at<uchar>(i,j)==255){
        dilate.at<uchar>(i,j)=0;}
        else{
        dilate.at<uchar>(i,j)=255;  
    }   }
}


//imshow("dilate1",dilate);
Rect doc=getComponents(grayscaleImg,dilate);



//Rect doc=getComponents(grayscaleImg,prob_houghLines);

cout<<"---------------------------------"<<endl;  
vector<double> mean1=getMeanPixelValuesInnerRect(frame,doc);
vector<double> mean2=getMeanPixcelValuesOuterRect(frame,doc);


double DiffBlue= std::abs(mean1[0]-mean2[0]);
double DiffGreen= std::abs(mean1[1]-mean2[1]);
double DiffRed= std::abs(mean1[2]-mean2[2]);

//cout<<"------------mean Diffrence------------"<<endl;
//cout<<DiffBlue <<" "<<DiffGreen << " "<<DiffRed<<endl;

Mat channel[3];
split(frame, channel);
//imshow("frame",frame);
if(DiffBlue>DiffRed && DiffBlue>DiffGreen ){
Mat cannyimg=GetCannyImage(channel[0],75,3,5);
//Mat prob_houghLines=Probabilistic_houghLines(cannyimg,5,100,100); 
//Rect doc=getComponents(grayscaleImg,prob_houghLines);

Mat dilate;
Mat element = getStructuringElement(cv::MORPH_CROSS,
              cv::Size(17,17),
              cv::Point(9, 9));
 
cv::dilate(cannyimg,dilate,element);

Mat d=dilate.clone();
for(int i=0;i<dilate.rows;i++){
 for(int j=0;j<dilate.cols;j++){
        if(dilate.at<uchar>(i,j)==255){
        dilate.at<uchar>(i,j)=0;}
        else{
        dilate.at<uchar>(i,j)=255;  
    }   }
}

//imshow("dilate",dilate);

Rect doc=getComponents(grayscaleImg,dilate);
DocRectangle.push_back(doc);

return doc;

}
else if(DiffGreen>DiffBlue && DiffGreen>DiffRed){

Mat cannyimg=GetCannyImage(channel[1],75,3,5);
//Mat prob_houghLines=Probabilistic_houghLines(cannyimg,5,100,100); 

Mat dilate;
Mat element = getStructuringElement(cv::MORPH_CROSS,
              cv::Size(17,17),
              cv::Point(9, 9));
 
cv::dilate(cannyimg,dilate,element); 
Mat d=dilate.clone();
for(int i=0;i<dilate.rows;i++){
 for(int j=0;j<dilate.cols;j++){
        if(dilate.at<uchar>(i,j)==255){
        dilate.at<uchar>(i,j)=0;}
        else{
        dilate.at<uchar>(i,j)=255;  
    }   }
}

//imshow("dilate",dilate);
Rect doc=getComponents(grayscaleImg,dilate);

//Rect doc=getComponents(grayscaleImg,prob_houghLines);
DocRectangle.push_back(doc);
return doc;
}

else if(DiffRed>DiffBlue && DiffRed>DiffGreen){

Mat cannyimg=GetCannyImage(channel[2],75,3,5);
//Mat prob_houghLines=Probabilistic_houghLines(cannyimg,5,100,100); 

Mat dilate;
Mat element = getStructuringElement(cv::MORPH_CROSS,
              cv::Size(17,17),
              cv::Point(9, 9));
 
cv::dilate(cannyimg,dilate,element); 
Mat d=dilate.clone();
for(int i=0;i<dilate.rows;i++){
 for(int j=0;j<dilate.cols;j++){
        if(dilate.at<uchar>(i,j)==255){
        dilate.at<uchar>(i,j)=0;}
        else{
        dilate.at<uchar>(i,j)=255;  
    }   }
}
//imshow("dilate",dilate);
Rect doc=getComponents(grayscaleImg,dilate);

DocRectangle.push_back(doc);

return doc;
}
}catch(exception e){
Rect r;
r.x=0;
r.y=0;
r.width=0;
r.height=0;
return r;
}

}


Rect SmartDoc::getComponents(Mat &Grayimg, Mat &img){

try{
    float max_x=0.8; 
    float max_y=0.8;
    float min_area=10;
    int type=8;
    std::vector<cv::Rect> rboxes,rboxes1;
    findConComp(img, rboxes, max_x, max_y, min_area, type);
 
    double areaPreviousFrame=0;
    double areaFrame=0;
    double Subtratedarea=0;


//imshow("img1112123",img);
   
    Mat comps;
    vector<Mat> channels;
    Mat red = Grayimg.clone();
    Mat green = Grayimg.clone();
    Mat blue = Grayimg.clone();
    channels.push_back(blue);
    channels.push_back(green);
    channels.push_back(red);
    merge(channels, comps);
    Mat c1=comps.clone();
    double max_area = 0.0;
    Rect doc;
    int docI;
    double MaxIntersectedArea=0;
    for(int i=0; i< rboxes.size(); i++){

            rectangle(comps, rboxes[i], Scalar(0,0,255), 3);

            double a = rboxes[i].width * rboxes[i].height;
            if(DocRectangle.size()>0){
        
 
                Rect IntersectedDoc=DocRectangle[DocRectangle.size()-1] & rboxes[i];
                double IntersectedArea=IntersectedDoc.width*IntersectedDoc.height;

                if(MaxIntersectedArea<IntersectedArea){
                    MaxIntersectedArea=IntersectedArea;
                    doc = rboxes[i];
                    cout<<doc.x<<" "<<doc.y<<endl;
                }

                }
            else{
                if(a > max_area){
                    doc = rboxes[i];
                    max_area = a;
                    docI=i;
                    }
                }
            }   
  
   if(DocRectangle.size()>0){
      areaPreviousFrame=DocRectangle[DocRectangle.size()-1].width*DocRectangle[DocRectangle.size()-1].height;
      areaFrame=doc.width*doc.height;
         Subtratedarea=areaFrame-areaPreviousFrame;
   }
    


//imshow("img00",img);
   
    cout<<areaPreviousFrame << " - "<<areaFrame<<"= ----"<<Subtratedarea<<endl;
    Mat prob_houghLines;
   
        for(int i=0;i<img.rows;i++){
 for(int j=0;j<img.cols;j++){
        if(img.at<uchar>(i,j)==255){
        img.at<uchar>(i,j)=0;}
        else{
        img.at<uchar>(i,j)=255;  
    }   }
}

cout<<(areaFrame*100)/areaPreviousFrame<<endl;

    if((areaFrame*100)/areaPreviousFrame<80){
       cout<<"Using hough =============="<<endl;

    prob_houghLines=Probabilistic_houghLines(img,5,100,100); 
    findConComp(prob_houghLines, rboxes1, max_x, max_y, min_area, type);
 


               for(int i=0; i< rboxes1.size(); i++){

                    rectangle(c1, rboxes1[i], Scalar(0,0,255), 3);

                    double a = rboxes1[i].width * rboxes1[i].height;
                    if(DocRectangle.size()>0){
        
                        Rect IntersectedDoc=DocRectangle[DocRectangle.size()-1] & rboxes1[i];
                        double IntersectedArea=IntersectedDoc.width*IntersectedDoc.height;

                        if(MaxIntersectedArea<IntersectedArea){
                             MaxIntersectedArea=IntersectedArea;
                            doc = rboxes1[i];
                            }

                    }
                    else{
                        if(a > max_area){
                            doc = rboxes1[i];
                            max_area = a;
                            docI=i;
                         }
                    }
                 } 
       
       //imshow("houghlines",prob_houghLines); 
            
            }
//imshow("comps",comps);
//imshow("img",img); 
//imshow("c1",c1);

//Mat f=img(doc);
/*if(f.rows >0 && f.cols>0){
imshow("F",f);    
}else{
    cout<<doc.x <<" ---"<<doc.y;
}
*/
//imshow("img11",img);
 //rectangle(comps, doc, Scalar(0,255,255), 3);
//Mat f=comps(doc);
//imshow("img",f); 
return doc;
}catch(exception e){
Rect r;
r.x=0;
r.y=0;
r.width=0;
r.height=0;
return r;
}



}

void SmartDoc::processFrame(Mat &frame, cv::Mat &processedFrame, int index)
{
	//cout<<"-----------------------"<<endl;
    // Clip card region 
    Mat grayscaleImg;
    cvtColor(frame, grayscaleImg, COLOR_BGR2GRAY);
   int MORPH = 9
   int CANNY = 84
   int HOUGH = 25

	Mat img = cv::cvtColor(orig, cv2.COLOR_BGR2GRAY);

    cv::GaussianBlur(img, img, cv::Size(3,3), 0);
    Mat kernel = cv::getStructuringElement(cv2.MORPH_RECT,(MORPH,MORPH));
    Mat dilated = cv::dilate(img, kernel);


   	Mat cannyImage;
    cv::Canny( dilated, cannyImage, 0, CANNY, 3 );
	
	vector<Vec4i> lines;
	HoughLinesP(cannyImage, lines, 1,  3.14/180, HOUGH);
	Mat pImg = Mat::zeros(imgMATOrig.rows, imgMATOrig.cols, CV_8UC1);
	for( size_t i = 0; i < lines.size(); i++ )
	{
		Vec4i l = lines[i];
		line( pImg, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255), 20, LINE_AA);
	}
	//imshow("Probabilistic_houghLines canny", imgMATOrig);
	pImg = 255 - pImg;


    // cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Point(9,9));
    // cv::Mat dilated;
    // cv::dilate(grayscaleImg, grayscaleImg, kernel);



     Mat imgBin; //= grayscaleImg.clone(); 
    binarizeOtsu(grayscaleImg, imgBin);

    //BackgroundSubtractorMOG();

    //imshow("Otsu",imgBin);
    //waitKey(0);
 	
  	//Rect document = getBounds(imgBin);
   	
    //Rect doc=ProcessImage_MeanValue(frame, grayscaleImg);
    
    /*Mat croppedImage;
    //If rect out of bounds
    try
    {
        croppedImage = frame(doc);
    }
    catch(exception e)
    {
        //Set the top left of the document to zero
        doc.x = 0;
        doc.y = 0;

        croppedImage = frame.clone();
    }

    Mat croppedImageGray;
    cvtColor(croppedImage, croppedImageGray, COLOR_BGR2GRAY);

    int ch=GetChannel(frame,doc);
    //cout<<"channel "<<ch<endl;
    Mat channel[3];
    split(croppedImage, channel);

    Mat canny = GetCannyImage(channel[ch], 50, 3, 5);
	Mat houghlinesImg = Probabilistic_houghLines(canny,75,200,75);
    //croppedImage = Std_houghLines(croppedImage, 175);


    //imshow("houghlinesImg",houghlinesImg);
    vector<Point2f> corners;
    detectCorners(houghlinesImg, corners,doc,frame);  

 
    //Save the bounding box in an XML file for Challenge 1
    if(CHALLENGE == 1)
    {
        //Write bounding box to file
        appendDocumentCoordinatesInXMLFile(doc, corners, index);
    }   */







    imshow("Corners", croppedImage);
    waitKey(50);

/*    char c = waitKey(0);
    if(c == 'q')
    {
        exit(0);
    }*/

    return;
}


/** 
 * This function implements a simple algorithm for document corner detection 
 *  
 * @param[in]     Cropped Binary image 
 * @param[in]     vector to store the coordinates of the detected corners 
 * @param[out]    None
 */
void SmartDoc::detectCorners(Mat& img, vector<Point2f> &corners, Rect doc,Mat frame)
{
    int segmentThreshold = img.cols / 2;

    Point2f upperLeftCorner, upperRightCorner;
    Point2f lowerLeftCorner, lowerRightCorner;

    bool breakLoop = false;

    //Detect upper left corner
    for(int row = 0; row < img.rows; row++)
    {
        for(int column = 0; column < segmentThreshold; column++)
        {
            if(img.at<uchar>(row, column) == 0)
            {
                //Iterate until the end
                while(1)
                {
                    if((column > 0) && (img.at<uchar>(row, column - 1) == 0))
                    {
                        column--;
                    }
                    else if((column > 0) && (row > 0) && (img.at<uchar>(row - 1, column - 1) == 0))
                    {
                        row--;
                        column--;
                    }
                    else if((img.at<uchar>(row + 1, column - 1) == 0))
                    {
                        row++;
                        column--;
                    }
                    else
                    {
                        //cout << "row " << row << endl << "column " << column << endl;
                        upperLeftCorner.x = column;
                        upperLeftCorner.y = row;
                        break;
                    }
                }

                breakLoop = true;
                break;
            }
        }

        if(breakLoop)
        {
            break;
        }
    }

    breakLoop = false;
    //Detect upper right corner
    for(int row = 0; row < img.rows; row++)
    {
        for(int column = img.cols - 1; column > segmentThreshold; column--)
        {
            if(img.at<uchar>(row, column) == 0)
            {
                //Iterate until the end
                while(1)
                {
                    if((column < (img.cols - 1)) && (img.at<uchar>(row, column + 1) == 0))
                    {
                        column++;
                    }
                    else if((column < (img.cols - 1)) && (row > 0) && (img.at<uchar>(row - 1, column + 1) == 0))
                    {
                        row--;
                        column++;
                    }
                    else if((img.at<uchar>(row + 1, column + 1) == 0))
                    {
                        row++;
                        column++;
                    }
                    else
                    {
                        //cout << "row " << row << endl << "column " << column << endl;
                        upperRightCorner.x = column;
                        upperRightCorner.y = row;
                        break;
                    }
                }

                breakLoop = true;
                break;
            }
        }

        if(breakLoop)
        {
            break;
        }
    }

    //Determine slope of the line
    //int m = (upperRightCorner.y - upperLeftCorner.y) / (upperRightCorner.x - upperLeftCorner.x);
    //cout << " ~~~~~~ " << m << " ~~~~~~ " << endl;


    breakLoop = false;
    //Detect lower left corner
    for(int row = img.rows - 1; row > 0; row--)
    {
        for(int column = 0; column < segmentThreshold; column++)
        {
            if(img.at<uchar>(row, column) == 0)
            {
                //Iterate until the end
                while(1)
                {
                    if((column > 0) && (img.at<uchar>(row, column - 1) == 0))
                    {
                        column--;
                    }
                    else if((column > 0) && (img.at<uchar>(row - 1, column - 1) == 0))
                    {
                        row--;
                        column--;
                    }
                    else if((column > 0) && (row < (img.rows - 1)) && (img.at<uchar>(row + 1, column - 1) == 0))
                    {
                        row++;
                        column--;
                    }
                    else
                    {
                        //cout << "row " << row << endl << "column " << column << endl;
                        lowerLeftCorner.x = column;
                        lowerLeftCorner.y = row;
                        break;
                    }
                }

                breakLoop = true;
                break;
            }
        }

        if(breakLoop)
        {
            break;
        }
    }

    breakLoop = false;
    //Detect lower right corner
    for(int row = img.rows - 1; row > 0; row--)
    {
        for(int column = img.cols - 1; column > segmentThreshold; column--)
        {
            if(img.at<uchar>(row, column) == 0)
            {
                //Iterate until the end
                while(1)
                {
                    if((column < (img.cols - 1)) && (img.at<uchar>(row, column + 1) == 0))
                    {
                        column++;
                    }
                    else if((column < (img.cols - 1)) && (img.at<uchar>(row - 1, column + 1) == 0))
                    {
                        row--;
                        column++;
                    }
                    else if((column < (img.cols - 1)) && (row < (img.rows - 1)) && (img.at<uchar>(row + 1, column + 1) == 0))
                    {
                        row++;
                        column++;
                    }
                    else
                    {
                        //cout << "row " << row << endl << "column " << column << endl;
                        lowerRightCorner.x = column;
                        lowerRightCorner.y = row;
                        break;
                    }
                }

                breakLoop = true;
                break;
            }
        }

        if(breakLoop)
        {
            break;
        }
    }


    corners.push_back(upperLeftCorner);
    corners.push_back(upperRightCorner);
    corners.push_back(lowerLeftCorner);
    corners.push_back(lowerRightCorner);

    Boundingbox boxCorner;
    boxCorner.topLeftPt=upperLeftCorner;
    boxCorner.topRightPt=upperRightCorner;
    boxCorner.BottomLeftPt=lowerLeftCorner;
    boxCorner.BottomRightPt=lowerRightCorner;
    boxCorner.ImageRect=doc;

    boxCorners.push_back(boxCorner);

    Mat cornersOutput = frame.clone();
    //Plot the points onto the image
    for(int i = 0; i < corners.size(); i++)
    {
        //Draw circles
        ellipse(cornersOutput, Point(corners[i].x+doc.x,corners[i].y+doc.y), Size(10,10), 0, 0 , 360, Scalar( 128, 0, 0 ), 3, 1);
    }

    //imshow("Corners Output", cornersOutput);
    //waitKey(0);
}



void SmartDoc::fixedCordinates(Mat &frame,int FrameNo)
{
    Mat frame1=frame.clone();

    int NoOftotalAvgFrameY=0;
    int NoOftotalAvgFrameX=0;


    Point AvgtopLeftPt;
    Point AvgtopRightPt;
    Point AvgBottomRightPt;
    Point AvgBottomLeftPt;
    int frameConsider=5;  // Number of frame on the one side
    int TotalFrameConsiders=2*frameConsider+1; //total frames on both sides 


    if(FrameNo-frameConsider>0){


	/*
    AvgtopLeftPt.x= AvgtopLeftPt.x+0.5*boxCorners[FrameNo].topLeftPt.x;
    AvgtopLeftPt.y=AvgtopLeftPt.y+0.5*boxCorners[FrameNo].topLeftPt.y;

	*/

	///// Frames Before the current frame/////////

	cout<<"AvgtopLeftPt "<<endl;
	double weitage=0.5;

	for(int i=1;i<=frameConsider;i++){
	weitage=weitage/2;

    AvgtopLeftPt.x= AvgtopLeftPt.x+weitage*boxCorners[FrameNo-i].topLeftPt.x;
    AvgtopLeftPt.y=AvgtopLeftPt.y+weitage*boxCorners[FrameNo-i].topLeftPt.y;

	cout<<boxCorners[FrameNo-i].topLeftPt<< "---";
    circle(frame1,Point(boxCorners[FrameNo].ImageRect.x+ boxCorners[FrameNo-i].topLeftPt.x,boxCorners[FrameNo].ImageRect.y+ boxCorners[FrameNo-i].topLeftPt.y), 5, Scalar(0,255,0), -1); 
	}

	cout<<endl<<"AvgtopRightPt"<<endl;
	weitage=0.5;

	for(int i=1;i<=frameConsider;i++){
		weitage=weitage/2;
    	AvgtopRightPt.x=AvgtopRightPt.x+weitage*boxCorners[FrameNo-i].topRightPt.x;
		AvgtopRightPt.y=AvgtopRightPt.y+weitage*boxCorners[FrameNo-i].topRightPt.y;
		cout<<boxCorners[FrameNo-i].topRightPt<< "---";

		circle(frame1,Point(boxCorners[FrameNo].ImageRect.x+ boxCorners[FrameNo-i].topRightPt.x,boxCorners[FrameNo].ImageRect.y+ boxCorners[FrameNo-i].topRightPt.y), 5, Scalar(0,255,0), -1);

	}


	cout<<endl<<"AvgBottomRightPt"<<endl;
	weitage=0.5;

	for(int i=1;i<=frameConsider;i++){

		weitage=weitage/2;
		AvgBottomRightPt.x=AvgBottomRightPt.x+weitage*boxCorners[FrameNo-i].BottomRightPt.x;//}
		AvgBottomRightPt.y=AvgBottomRightPt.y+weitage*boxCorners[FrameNo-i].BottomRightPt.y;//}
		cout<<boxCorners[FrameNo-i].BottomRightPt<< "---";

		circle(frame1,Point(boxCorners[FrameNo].ImageRect.x+ boxCorners[FrameNo-i].BottomRightPt.x,boxCorners[FrameNo].ImageRect.y+ boxCorners[FrameNo-i].BottomRightPt.y), 5, Scalar(0,255,0), -1);
	}


	cout<<endl<<"AvgBottomLeftPt"<<endl;
	weitage=0.5;

	for(int i=1;i<=frameConsider;i++){

		weitage=weitage/2;
		AvgBottomLeftPt.x=AvgBottomLeftPt.x+weitage*boxCorners[FrameNo-i].BottomLeftPt.x;
		AvgBottomLeftPt.y=AvgBottomLeftPt.y+weitage*boxCorners[FrameNo-i].BottomLeftPt.y;
		cout<<boxCorners[FrameNo-i].BottomLeftPt<< "---";

		circle(frame1,Point(boxCorners[FrameNo].ImageRect.x+ boxCorners[FrameNo-i].BottomLeftPt.x,boxCorners[FrameNo].ImageRect.y+ boxCorners[FrameNo-i].BottomLeftPt.y), 5, Scalar(0,255,0), -1);
	}



	/////////////////////////////////// Frames After the current frame/////////////////////////////////

if(FrameNo+frameConsider<boxCorners.size()){

	cout<<endl<<"AvgtopLeftPt"<<endl;
	weitage=0.5;
	weitage=weitage/2;

	for(int i=frameConsider;i>0;i--){

		AvgtopLeftPt.x=AvgtopLeftPt.x+weitage*boxCorners[FrameNo+i].topLeftPt.x;
		AvgtopLeftPt.y=AvgtopLeftPt.y+weitage*boxCorners[FrameNo+i].topLeftPt.y;

		cout<<boxCorners[FrameNo+i].topLeftPt<< "---";
		circle(frame1,Point(boxCorners[FrameNo].ImageRect.x+ boxCorners[FrameNo+i].topLeftPt.x,boxCorners[FrameNo].ImageRect.y+ boxCorners[FrameNo+i].topLeftPt.y), 5, Scalar(0,255,0), -1);

	}

	cout<<endl<<"AvgtopRightPt"<<endl;
	weitage=0.5;

	for(int i=frameConsider;i>0;i--){
	weitage=weitage/2;

		AvgtopRightPt.x=AvgtopRightPt.x+weitage*boxCorners[FrameNo+i].topRightPt.x;
		AvgtopRightPt.y=AvgtopRightPt.y+weitage*boxCorners[FrameNo+i].topRightPt.y;

		cout<<boxCorners[FrameNo+i].topRightPt<< "---";
		circle(frame1,Point(boxCorners[FrameNo].ImageRect.x+ boxCorners[FrameNo+i].topRightPt.x,boxCorners[FrameNo].ImageRect.y+ boxCorners[FrameNo+i].topRightPt.y), 5, Scalar(0,255,0), -1);

	}

	cout<<endl;

	cout<<"AvgBottomRightPt"<<endl;
	weitage=0.5;

	for(int i=frameConsider;i>0;i--){
		weitage=weitage/2;
		AvgBottomRightPt.x=AvgBottomRightPt.x+weitage*boxCorners[FrameNo+i].BottomRightPt.x;
		AvgBottomRightPt.y=AvgBottomRightPt.y+weitage*boxCorners[FrameNo+i].BottomRightPt.y;

		cout<<boxCorners[FrameNo+i].BottomRightPt<< "---";
		circle(frame1,Point(boxCorners[FrameNo].ImageRect.x+ boxCorners[FrameNo+i].BottomRightPt.x,boxCorners[FrameNo].ImageRect.y+ boxCorners[FrameNo+i].BottomRightPt.y), 5, Scalar(0,255,0), -1);

}

cout<<endl<<"AvgBottomLeftPt"<<endl;
weitage=0.5;

for(int i=frameConsider;i>0;i--){
weitage=weitage/2;
AvgBottomLeftPt.x=AvgBottomLeftPt.x+weitage*boxCorners[FrameNo+i].BottomLeftPt.x;
AvgBottomLeftPt.y=AvgBottomLeftPt.y+weitage*boxCorners[FrameNo+i].BottomLeftPt.y;

cout<<boxCorners[FrameNo+i].BottomLeftPt<< "---";
circle(frame1,Point(boxCorners[FrameNo].ImageRect.x+ boxCorners[FrameNo+i].BottomLeftPt.x,boxCorners[FrameNo].ImageRect.y+ boxCorners[FrameNo+i].BottomLeftPt.y), 5, Scalar(0,255,0), -1);

}
cout<<endl;

}

cout<<"sum AvgtopLeftPt.x "<<AvgtopLeftPt.x<<endl;
cout<<"sum AvgtopLeftPt.y "<<AvgtopLeftPt.y<<endl;
cout<<"sum AvgtopRightPt.x "<<AvgtopRightPt.x<<endl;
cout<<"sum AvgtopRightPt.y "<<AvgtopRightPt.y<<endl;
cout<<"sum AvgBottomRightPt.x "<<AvgBottomRightPt.x<<endl;
cout<<"sum AvgBottomRightPt.y " <<AvgBottomRightPt.y<<endl;
cout<<"sum AvgBottomLeftPt.x "<<AvgBottomLeftPt.x<<endl;
cout<<"sum AvgBottomLeftPt.y "<<AvgBottomLeftPt.y<<endl;

/*

AvgtopLeftPt.x=AvgtopLeftPt.x/5;
AvgtopLeftPt.y=AvgtopLeftPt.y/5;

AvgtopRightPt.x=AvgtopRightPt.x/5;
AvgtopRightPt.y=AvgtopRightPt.y/5;

AvgBottomRightPt.x=AvgBottomRightPt.x/5;
AvgBottomRightPt.y=AvgBottomRightPt.y/5;

AvgBottomLeftPt.x=AvgBottomLeftPt.x/5;
AvgBottomLeftPt.y=AvgBottomLeftPt.y/5;
*/
cout<<"AvgtopLeftPt"<<AvgtopLeftPt<<endl;
cout<<"AvgtopRightPt "<<AvgtopRightPt<<endl;

cout<<"AvgBottomRightPt "<<AvgBottomRightPt<<endl;
cout<<"AvgBottomLeftPt "<<AvgBottomLeftPt<<endl;


circle(frame1, Point(boxCorners[FrameNo].topLeftPt.x+boxCorners[FrameNo].ImageRect.x,boxCorners[FrameNo].topLeftPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(0,0,255), -1);
circle(frame1, Point(boxCorners[FrameNo].topRightPt.x+boxCorners[FrameNo].ImageRect.x,boxCorners[FrameNo].topRightPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(0,0,255), -1);
circle(frame1, Point(boxCorners[FrameNo].BottomLeftPt.x+boxCorners[FrameNo].ImageRect.x,boxCorners[FrameNo].BottomLeftPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(0,0,255), -1);
circle(frame1, Point(boxCorners[FrameNo].BottomRightPt.x+boxCorners[FrameNo].ImageRect.x,boxCorners[FrameNo].BottomRightPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(0,0,255), -1);



circle(frame1, Point(AvgtopLeftPt.x+boxCorners[FrameNo].ImageRect.x,AvgtopLeftPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(255,255,0), -1);
circle(frame1, Point(AvgtopRightPt.x+boxCorners[FrameNo].ImageRect.x,AvgtopRightPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(255,255,0), -1);
circle(frame1, Point(AvgBottomRightPt.x+boxCorners[FrameNo].ImageRect.x,AvgBottomRightPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(255,255,0), -1);
circle(frame1, Point(AvgBottomLeftPt.x+boxCorners[FrameNo].ImageRect.x,AvgBottomLeftPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(255,255,0), -1);



cout<<"Frame No = "<<FrameNo<<endl;
cout<<"Before"<<endl;
cout<<boxCorners[FrameNo].topLeftPt<<endl;
cout<<boxCorners[FrameNo].topRightPt<<endl;
cout<<boxCorners[FrameNo].BottomRightPt<<endl;
cout<<boxCorners[FrameNo].BottomLeftPt<<endl;



////////////////////////////////////// Finding Average Distances From the Average Point(center Point)///////////////////////////////

double AvgtopLeftdist=0;
double AvgtopRightdist=0;

double AvgBottomLeftdist=0;
double AvgBottomRightdist=0;

double topLeftdist,topRightdist,BottomLeftdist,BottomRightdist;


for(int i=0;i<TotalFrameConsiders;i++){
AvgtopLeftdist=AvgtopLeftdist + std::sqrt(std::pow(boxCorners[FrameNo-frameConsider+i].topLeftPt.x - AvgtopLeftPt.x,2) + (std::pow(boxCorners[FrameNo-frameConsider+i].topLeftPt.y - AvgtopLeftPt.y,2)));

}


for(int i=0;i<TotalFrameConsiders;i++){

AvgtopRightdist=AvgtopRightdist + std::sqrt(std::pow(boxCorners[FrameNo-frameConsider+i].topRightPt.x - AvgtopRightPt.x,2) + (std::pow(boxCorners[FrameNo-frameConsider+i].topRightPt.y - AvgtopRightPt.y,2)));

}

for(int i=0;i<TotalFrameConsiders;i++){

AvgBottomLeftdist=AvgBottomLeftdist + std::sqrt(std::pow(boxCorners[FrameNo-frameConsider+i].BottomLeftPt.x - AvgBottomLeftPt.x,2) + (std::pow(boxCorners[FrameNo-frameConsider+i].BottomLeftPt.y - AvgBottomLeftPt.y,2)));

}


for(int i=0;i<TotalFrameConsiders;i++){

AvgBottomRightdist=AvgBottomRightdist + std::sqrt(std::pow(boxCorners[FrameNo-frameConsider+i].BottomRightPt.x - AvgBottomRightPt.x,2) + (std::pow(boxCorners[FrameNo-frameConsider+i].BottomRightPt.y - AvgBottomRightPt.y,2)));

}



AvgtopRightdist=AvgtopRightdist /TotalFrameConsiders;
AvgtopLeftdist=AvgtopLeftdist /TotalFrameConsiders;

AvgBottomRightdist=AvgBottomRightdist/TotalFrameConsiders;
AvgBottomLeftdist=AvgBottomLeftdist/TotalFrameConsiders;



////////////////////////////////////// Finding Distances Between the Current Point and Center Point///////////////////////////////

topRightdist=std::sqrt(std::pow(boxCorners[FrameNo].topRightPt.x - AvgtopRightPt.x,2) + (std::pow(boxCorners[FrameNo].topRightPt.y - AvgtopRightPt.y,2)));
topLeftdist=std::sqrt(std::pow(boxCorners[FrameNo].topLeftPt.x - AvgtopLeftPt.x,2) + (std::pow(boxCorners[FrameNo].topLeftPt.y - AvgtopLeftPt.y,2)));
	
BottomRightdist=std::sqrt(std::pow(boxCorners[FrameNo].BottomRightPt.x - AvgBottomRightPt.x,2) + (std::pow(boxCorners[FrameNo].BottomRightPt.y - AvgBottomRightPt.y,2)));
BottomLeftdist=std::sqrt(std::pow(boxCorners[FrameNo].BottomLeftPt.x - AvgBottomLeftPt.x,2) + (std::pow(boxCorners[FrameNo].BottomLeftPt.y - AvgBottomLeftPt.y,2)));


////////////////////////////////// Replacing current point with new point(closest to the center point) if distance is less than average distance/////////////


				
if(topRightdist>AvgtopRightdist){

cout<<"AvgtopRightdist"<<AvgtopRightdist<<endl;
cout<<"topRightdist"<<topRightdist<<endl;

int mindist=999999;
int dist;
int Fno;

for(int i=0;i<TotalFrameConsiders;i++){
dist=std::sqrt(std::pow(boxCorners[FrameNo-frameConsider+i].topRightPt.x - AvgtopRightPt.x,2) + (std::pow(boxCorners[FrameNo-5+i].topRightPt.y - AvgtopRightPt.y,2)));

	if(dist<mindist)
	{
 	mindist= dist;
 	Fno=i;
	}
}
boxCorners[FrameNo].topRightPt.x=boxCorners[FrameNo-frameConsider+Fno].topRightPt.x;
boxCorners[FrameNo].topRightPt.y=boxCorners[FrameNo-frameConsider+Fno].topRightPt.y;
}


if(topLeftdist>AvgtopLeftdist){

cout<<"AvgtopLeftdist"<<AvgtopLeftdist<<endl;
cout<<"topLeftdist"<<topLeftdist<<endl;


int mindist=999999;
int dist;
int Fno;
for(int i=0;i<TotalFrameConsiders;i++){
dist=std::sqrt(std::pow(boxCorners[FrameNo-frameConsider+i].topLeftPt.x - AvgtopLeftPt.x,2) + (std::pow(boxCorners[FrameNo-frameConsider+i].topLeftPt.y - AvgtopLeftPt.y,2)));
if(dist<mindist)
{
 mindist= dist;
 Fno=i;
}
}
boxCorners[FrameNo].topLeftPt.x=boxCorners[FrameNo-frameConsider+Fno].topLeftPt.x;
boxCorners[FrameNo].topLeftPt.y=boxCorners[FrameNo-frameConsider+Fno].topLeftPt.y;
}


if(BottomLeftdist>AvgBottomLeftdist){

cout<<"AvgBottomLeftdist"<<AvgBottomLeftdist<<endl;
cout<<"BottomLeftdist"<<BottomLeftdist<<endl;

int mindist=999999;
int dist;
int Fno;
for(int i=0;i<TotalFrameConsiders;i++){
dist=std::sqrt(std::pow(boxCorners[FrameNo-frameConsider+i].BottomLeftPt.x - AvgBottomLeftPt.x,2) + (std::pow(boxCorners[FrameNo-5+i].BottomLeftPt.y - AvgBottomLeftPt.y,2)));
if(dist<mindist)
{
 mindist= dist;
 Fno=i;
}
}
boxCorners[FrameNo].BottomLeftPt.x=boxCorners[FrameNo-frameConsider+Fno].BottomLeftPt.x;
boxCorners[FrameNo].BottomLeftPt.y=boxCorners[FrameNo-frameConsider+Fno].BottomLeftPt.y;

}


if(BottomRightdist>AvgBottomRightdist){
cout<<"AvgBottomRightdist"<<AvgBottomRightdist<<endl;
cout<<"BottomRightdist"<<BottomRightdist<<endl;

int mindist=999999;
int dist;
int Fno;

for(int i=0;i<TotalFrameConsiders;i++){
dist=std::sqrt(std::pow(boxCorners[FrameNo-frameConsider+i].BottomRightPt.x - AvgBottomRightPt.x,2) + (std::pow(boxCorners[FrameNo-5+i].BottomRightPt.y - AvgBottomRightPt.y,2)));

cout<<"dist"<<dist<<" "<<boxCorners[FrameNo-frameConsider+i].BottomRightPt<<endl;
if(dist<mindist)
{
 mindist= dist;
 Fno=i;
}
}

boxCorners[FrameNo].BottomRightPt.x=boxCorners[FrameNo-frameConsider+Fno].BottomRightPt.x;
boxCorners[FrameNo].BottomRightPt.y=boxCorners[FrameNo-frameConsider+Fno].BottomRightPt.y;

}





cout<<"After"<<endl;
cout<<boxCorners[FrameNo].topLeftPt<<endl;
cout<<boxCorners[FrameNo].topRightPt<<endl;
cout<<boxCorners[FrameNo].BottomRightPt<<endl;
cout<<boxCorners[FrameNo].BottomLeftPt<<endl;

}
circle(frame1, Point(boxCorners[FrameNo].topLeftPt.x+boxCorners[FrameNo].ImageRect.x,boxCorners[FrameNo].topLeftPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(0,255,255), -1);
circle(frame1, Point(boxCorners[FrameNo].topRightPt.x+boxCorners[FrameNo].ImageRect.x,boxCorners[FrameNo].topRightPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(0,255,255), -1);
circle(frame1, Point(boxCorners[FrameNo].BottomLeftPt.x+boxCorners[FrameNo].ImageRect.x,boxCorners[FrameNo].BottomLeftPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(0,255,255), -1);
circle(frame1, Point(boxCorners[FrameNo].BottomRightPt.x+boxCorners[FrameNo].ImageRect.x,boxCorners[FrameNo].BottomRightPt.y+boxCorners[FrameNo].ImageRect.y), 5, Scalar(0,255,255), -1);




//Mat frame1=frame.clone();


/*
circle(frame1, boxCorners[FrameNo].topLeftPt, 5, Scalar(0,0,255), -1);
circle(frame1, boxCorners[FrameNo].topRightPt, 5, Scalar(0,0,255), -1);
circle(frame1, boxCorners[FrameNo].BottomLeftPt, 5, Scalar(0,0,255), -1);
circle(frame1, boxCorners[FrameNo].BottomRightPt, 5, Scalar(0,0,255), -1);
*/
//imshow("corners",frame1);


}








/** 
 * This function implements global image thresholding using Otsu's algorithm 
 *  
 * @param[in]     gray Gray scale input image as an OpenCV Mat object. 
 * @param[out]    binary Output image binarized using Otsu's method. 
 */ 
void SmartDoc::binarizeOtsu(Mat &gray, Mat &binary)
{ 
	//Apply Guassian Blur (3x3)
	//blur( gray, gray, Size(3,3) );

	//Opening
	//erode(gray, gray, Mat());
    //dilate(gray, gray, Mat());

	//Closing
    //dilate(gray, gray, Mat());
    //erode(gray, gray, Mat());

	//Threshold = 0; Max_Val = 255
    threshold(gray, binary, 0, 255, THRESH_BINARY | THRESH_OTSU); 

}

/** 
 * This function implements local adaptive thresholding algorithms from: 
 * Faisal Shafait, Daniel Keysers, Thomas M. Breuel. "Efficient Implementation  
 * of Local Adaptive Thresholding Techniques Using Integral Images",  
 * SPIE Document Recognition and Retrieval XV, DRR'08, San Jose, CA, USA. Jan. 2008 
 *  
 * @param[in]     gray Gray scale input image as an OpenCV Mat object. 
 * @param[out]    binary Output image binarized using Shafait's method. 
 * @param[in]     w local square window side length to compute adaptive threshold. 
 * @param[in]     k Gray level sensititivity parameter. Lower values of k result in whiter images (fewer black pixels) and vice versa. 
 */ 
void SmartDoc::binarizeShafait(Mat &gray, Mat &binary, int w, double k)
{ 
    Mat sum, sumsq; 
    gray.copyTo(binary); 
    int half_width = w >> 1; 
    integral(gray, sum, sumsq,  CV_64F); 
    for(int i=0; i<gray.rows; i ++){ 
        for(int j=0; j<gray.cols; j++){ 
            int x_0 = (i > half_width) ? i - half_width : 0; 
            int y_0 = (j > half_width) ? j - half_width : 0; 
            int x_1 = (i + half_width >= gray.rows) ? gray.rows - 1 : i + half_width; 
            int y_1 = (j + half_width >= gray.cols) ? gray.cols - 1 : j + half_width; 
            double area = (x_1-x_0) * (y_1-y_0); 
            double mean = (sum.at<double>(x_0,y_0) + sum.at<double>(x_1,y_1) - sum.at<double>(x_0,y_1) - sum.at<double>(x_1,y_0)) / area; 
            double sq_mean = (sumsq.at<double>(x_0,y_0) + sumsq.at<double>(x_1,y_1) - sumsq.at<double>(x_0,y_1) - sumsq.at<double>(x_1,y_0)) / area; 
            double stdev = sqrt(sq_mean - (mean * mean)); 
            double threshold = mean * (1 + k * ((stdev / 128) -1) ); 
            if (gray.at<uchar>(i,j) > threshold) 
                binary.at<uchar>(i,j) = 255; 
            else 
                binary.at<uchar>(i,j) = 0; 
        } 
    } 
}

/** 
 * This function implements directory traversal using boost filesystem libraries
 *  
 * @param[in]     Name of the directory to be traversed 
 * @param[in]     Format of the files to be searched
 * @param[out]    Vector containing absolute paths of all the files
 */ 
void SmartDoc::getFilesFromDirectory(string dir, vector<string> &files, string format)
{
	path current_dir(dir);

	for (recursive_directory_iterator iter(current_dir), end; iter != end; ++iter)
	{
		string name = iter->path().filename().string();
		//cout << iter->path() << endl;
		if (boost::ends_with(name, format))
		{
			files.push_back(iter->path().string());
		}
	}
}


/** 
 * This function initializes the XML file by writing its basic structure
 *  
 * @param[in]     None
 * @param[out]    None
 */ 
void SmartDoc::initializeXMLFile(string directory)
{
    document.reset();
    //<?xml version='1.0' encoding='utf-8'?>

    //<ground_truth version="0.2" generated="2014-11-27T20:36:46.513598">
    //Add parent node
    xml_node ground_truth = document.append_child();
    ground_truth.set_name("seg_result");
    ground_truth.append_attribute("version") = "1.1";
    string dateAndTime = currentDateTime();
    //cout << dateAndTime << endl;
    ground_truth.append_attribute("generated") = dateAndTime.c_str();

    //<software_used name="GT builder with inpainting" version="0.4"/>
    xml_node software_used = ground_truth.append_child();
    software_used.set_name("software_used");
    software_used.append_attribute("name") = "SmartDoc";
    software_used.append_attribute("version") = "1.0";

    //<source_sample_file>background00/datasheet001.mp4</source_sample_file>
    xml_node source_sample_file = ground_truth.append_child();
    source_sample_file.set_name("source_sample_file");
    
    string temp = directory + "/" + videoFileName + ".mp4";
    cout << temp << endl;

    source_sample_file.append_child(node_pcdata).set_value(temp.c_str());

    //<segmentation_results>
    segmentation_results = ground_truth.append_child();
    segmentation_results.set_name("segmentation_results");
}

/** 
 * This function returns the current date and time
 *  
 * @param[in]     None
 * @param[out]    string containing the data and time in the specified format
 */ 
string SmartDoc::currentDateTime() 
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%dT%X", &tstruct);

    return buf;
}

/** 
 * This function writes the Rect coordinates provided to it in the XML file
 *  
 * @param[in]     Rect whose coordinates needs to be saved in the XML file
 * @param[out]    None
 */ 
void SmartDoc::appendDocumentCoordinatesInXMLFile(Rect document, vector<Point2f> corners, int index)
{
    /*
    //Insert the frames in the middle where the coordinates couldn't be found
    while(((lastFrameIndexStoredInXML + 1) != index) && (lastFrameIndexStoredInXML < index))
    {
        lastFrameIndexStoredInXML++;


        //Append the child with rejected = true
        xml_node frame = segmentation_results.append_child();
        frame.set_name("frame");
        frame.append_attribute("rejected") = "true";
        frame.append_attribute("index") = lastFrameIndexStoredInXML;
    }*/

    if(corners[0].x==0 && corners[0].y==0 && corners[1].x==0 && corners[1].y==0  && corners[2].x==0 && corners[2].y==0 && corners[3].x==0 && corners[3].y==0 ){
        xml_node frame = segmentation_results.append_child();
        frame.set_name("frame");
        frame.append_attribute("rejected") = "true";
        frame.append_attribute("index") = index;

        return;
    }
    

    //<frame index="1" rejected="false">
    xml_node frame = segmentation_results.append_child();
    frame.set_name("frame");
    frame.append_attribute("rejected") = "false";
    frame.append_attribute("index") = index;

    //Add coordinates
    //<point name="bl" x="716.625" y="805.609"/>
    xml_node point = frame.append_child();
    point.set_name("point");
    point.append_attribute("name") = "tl";
    point.append_attribute("x") = document.x + corners[0].x;
    point.append_attribute("y") = document.y + corners[0].y;

    point = frame.append_child();
    point.set_name("point");
    point.append_attribute("name") = "tr";
    point.append_attribute("x") = document.x + corners[1].x;
    point.append_attribute("y") = document.y + corners[1].y;

    point = frame.append_child();
    point.set_name("point");
    point.append_attribute("name") = "bl";
    point.append_attribute("x") = document.x + corners[2].x;
    point.append_attribute("y") = document.y + corners[2].y;

    point = frame.append_child();
    point.set_name("point");
    point.append_attribute("name") = "br";
    point.append_attribute("x") = document.x + corners[3].x;
    point.append_attribute("y") = document.y + corners[3].y;

    //lastFrameIndexStoredInXML = index;
}

bool SmartDoc::validateXML(int frames)
{
    xml_node seg_results = segmentation_results;

    int counter = 0;
    for (xml_node frame = seg_results.first_child(); frame; frame = frame.next_sibling())
    {
        counter++;
    }

    if(counter != frames)
    {
        cerr << "Frames written in XML are not equal to the number of frames in the video" << endl;
        exit(-1);
    }

    //pugi::xml_node panels = doc.child("frame");

    //std::cout << panels.name() << std::endl;

    /*
    for (pugi::xml_node panel = panels.first_child(); panel; panel = panel.next_sibling())
    {
        std::cout << panel.name() << std::endl;

        for (pugi::xml_attribute attr = panel.first_attribute(); attr; attr = attr.next_attribute())
        {
            std::cout << " " << attr.name() << "=" << attr.value() << std::endl;
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;*/
}



//Main Method
int main(int argc, char **argv)
{
    if(argc < 2){
        cerr << "Usage: " << argv[0] << " <path>\n";
        exit(-1);
    }
	SmartDoc smartDoc;

	if(CHALLENGE == 1)
	{
		smartDoc.readVideoFiles(argv[1]);
	}
	else
	{
		smartDoc.readImageFiles(argv[1]);
	}
}
