#ifndef SMART_DOC_H
#define SMART_DOC_H
 
#include <iostream>
#include <fstream>
#include <string> 
#include <vector> 
#include <time.h>
#include <stdio.h>

// Tesseract headers 
#include <tesseract/baseapi.h> 
#include <leptonica/allheaders.h> 

// Open CV headers 
#include "opencv2/core/core.hpp" 
#include "opencv2/videoio.hpp"
#include "opencv2/highgui/highgui.hpp" 
#include "opencv2/imgproc/imgproc.hpp" 
#include "opencv2/video/background_segm.hpp"

#include <stdexcept>
#include <tesseract/strngs.h>
#include <tesseract/osdetect.h> 
#include "opencv-tess-utils.h"

#include "pugixml.hpp"

#include <boost/filesystem.hpp>
#include <boost/algorithm/string/predicate.hpp>


//Preprocessor Directives
#define DEBUG 0
#define LINE_WIDTH 2
#define VIDEOFILESFORMAT "avi"
#define IMAGEFILESFORMAT "jpg"


class SmartDoc
{
private:
	std::string videoFileName;
	std::string imageFileName;
	pugi::xml_document document;
	pugi::xml_node segmentation_results;

public:
	//Function prototypes
	 void readVideoFiles(std::string path);
	 void basicDetection(cv::Mat& mat);
	 void readImageFiles(std::string path);
	 void backgroundSubtraction(cv::VideoCapture cap);
	 cv::Rect ProcessImage_MeanValue(cv::Mat &frame, cv::Mat &grayscaleImg);
	 cv::Rect getComponents(cv::Mat &Grayimg, cv::Mat &img);
	 void processFrame(cv::Mat &frame, cv::Mat &processedFrame, int index = 0);
	 void detectCorners(cv::Mat& img, std::vector<cv::Point2f> &corners, cv::Rect doc, cv::Mat frame);
	 void binarizeOtsu(cv::Mat &gray, cv::Mat &binary);
	 void binarizeShafait(cv::Mat &gray, cv::Mat &binary, int w, double k);
	 void getFilesFromDirectory(std::string dir, std::vector<std::string> &files, std::string format);
	 bool recognizeText(cv::Mat& documentImage, std::string filename);
	 void initializeXMLFile(std::string directory);
	 std::string currentDateTime();
	 void appendDocumentCoordinatesInXMLFile(cv::Rect document, std::vector<cv::Point2f> corners, int index);
	 bool validateXML(int frames);
	 int GetChannel(cv::Mat &frame, cv::Rect doc);

std::vector<double> getMeanPixelValuesInnerRect(cv::Mat &frame, cv::Rect doc);
std::vector<double> getMeanPixcelValuesOuterRect(cv::Mat &frame, cv::Rect doc);
void fixedCordinates(cv::Mat &frame,int frameNo);

};


#endif
