#include "smartDoc.h"

using namespace std;
using namespace tesseract;
using namespace cv;
using namespace pugi;
using namespace boost::filesystem;

string DEBUGFILESDIRECTORY = "/home/shoaib/SmartDoc/smartdoc/debug";
string OUTPUTXMLFILEDIRECTORY = "/home/shoaib/SmartDoc/smartdoc/XMLFiles/";
string TEXTFILESDIRECTORY = "/home/shoaib/SmartDoc/smartdoc/output/";

#define CHALLENGE 2			//Challenge 1 = Document Detection; Challenge 2 = Document OCR

Mat Getcontours(Mat cannyImage){

	//Boundary Detection using Countour Finding Method
	vector< vector<Point> > contoursCanny, contoursBinary;
	vector<Vec4i> hierarchy;
	findContours( cannyImage, contoursCanny, hierarchy, RETR_EXTERNAL,  CHAIN_APPROX_SIMPLE );
	//findContours( imgMATOrig, contoursBinary, hierarchy, RETR_EXTERNAL,  CHAIN_APPROX_SIMPLE );

	/// Approximate contours to polygons and get bounding rects
	vector< vector<Point> > contours_poly( contoursCanny.size() );
	vector<Rect> boundRect( contoursCanny.size() );
	vector< vector<Point> > contoursBin_poly( contoursBinary.size() );
	vector<Rect> boundRectBin( contoursBinary.size() );

	Rect maxAreaRect;
    double maxArea = 0;
    double area = 0;

  
    //Compute the area of the contours and keep track of the rect with the biggest area
	for( int i = 0; i < contoursCanny.size(); i++ )
	{ 
		approxPolyDP( Mat(contoursCanny[i]), contours_poly[i], 3, true );
		boundRect[i] = boundingRect( Mat(contours_poly[i]) );

		area = contourArea(contoursCanny[i]);
        if( area > maxArea )
        {
            maxArea = area;
            maxAreaRect = boundRect[i];
        }
	}

	for( int i = 0; i < contoursBinary.size(); i++ )
	{ 
		approxPolyDP( Mat(contoursBinary[i]), contoursBin_poly[i], 3, true );
		boundRectBin[i] = boundingRect( Mat(contoursBin_poly[i]) );

		area = contourArea(contoursBinary[i]);
        if( area > maxArea )
        {
            maxArea = area;
            maxAreaRect = boundRectBin[i];
        }
	}

	printf("Canny contours: %d, Binary contours: %d\n", int(contoursCanny.size()), int(contoursBinary.size()));


	/// Draw polygonal contour + bonding rects
	Mat drawing = Mat::zeros( cannyImage.size(), CV_8UC3 );
	for( int i = 0; i< contoursCanny.size(); i++ )
	{
		Scalar color = Scalar(255,0,255);
		drawContours( drawing, contours_poly, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
		rectangle( drawing, boundRect[i].tl(), boundRect[i].br(), color, 2, 8, 0 );          
	}
	//imshow("Contour Detector", drawing);
	return drawing;
}


Mat Probabilistic_houghLines(Mat imgMATOrig, int threshold, int minLineLength, int maxLineGap){
	//Boundary Detection using Probabilistic Hough Lines
	

	/*int minLineLength = 10;
	int maxLineGap = 100;
	int threshold = 5;*/	
		
	//Number of intersections to detect a line
	
	vector<Vec4i> lines;
	HoughLinesP(imgMATOrig, lines, 1, CV_PI/180, threshold, minLineLength, maxLineGap );
	Mat pImg = Mat::zeros(imgMATOrig.rows, imgMATOrig.cols, CV_8UC1);
	for( size_t i = 0; i < lines.size(); i++ )
	{
		Vec4i l = lines[i];
		line( pImg, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255), 3, LINE_AA);
	}
	//imshow("Hough Lines", imgMATOrig);
	pImg = 255 - pImg;
	return pImg;
}

Mat Std_houghLines(Mat houghLinesMat,int threshold){

	//Boundary Detection using Standard Hough Lines
	//int threshold = 5;
	
	vector<Vec2f> lines;
	HoughLines(houghLinesMat, lines, 1, CV_PI/180, threshold, 0, 0 );

	for( size_t i = 0; i < lines.size(); i++ )
	{
		float rho = lines[i][0], theta = lines[i][1];

		float thetaDeg = theta * 180;
		//printf("Theta %f and Theta(Deg) %f", theta, thetaDeg );

		//Check if the line is vertical or horizontal
		if( ((thetaDeg >= 0) && (thetaDeg < 10)) || ((thetaDeg > 170) && (thetaDeg < 200)) )
		{
			//printf("Range detected");
			Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a*rho, y0 = b*rho;
		pt1.x = cvRound(x0 + 1000*(-b));
		pt1.y = cvRound(y0 + 1000*(a));
		pt2.x = cvRound(x0 - 1000*(-b));
		pt2.y = cvRound(y0 - 1000*(a));
		line( houghLinesMat, pt1, pt2, Scalar(255,0,0), 3, LINE_AA);
		}

		Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a*rho, y0 = b*rho;
		pt1.x = cvRound(x0 + 1000*(-b));
		pt1.y = cvRound(y0 + 1000*(a));
		pt2.x = cvRound(x0 - 1000*(-b));
		pt2.y = cvRound(y0 - 1000*(a));
		line( houghLinesMat, pt1, pt2, Scalar(255,0,0), 3, LINE_AA);
	}

	//imshow("Standard Hough Lines", houghLinesMat);
    return houghLinesMat;
}



Mat GetCannyImage(Mat grayscaleImg,int lowThreshold,int ratio,int kernel_size){

	/// Reduce noise with a kernel 3x3
	blur( grayscaleImg, grayscaleImg, Size(15,15) );

/*	int lowThreshold = 75, ratio = 3;
	int kernel_size = 5;*/
	Mat cannyImage;

	/// Canny detector
	Canny( grayscaleImg, cannyImage, lowThreshold, lowThreshold*ratio, kernel_size );
//imshow("cannyimg Frame", cannyImage);
return cannyImage;
}

Mat lineSegmentDectector(Mat cannyImage){

		//Apply OpenCV 3 LineSegmentDetector
    Ptr<LineSegmentDetector> ls = createLineSegmentDetector(LSD_REFINE_STD);
    
    vector<Vec4i> lines_std;

    // Detect the lines
    ls->detect(cannyImage, lines_std);

    // Show found lines
    Mat drawnLines(cannyImage);
    ls->drawSegments(drawnLines, lines_std);
    //imshow("LSD Output", drawnLines);
    return drawnLines;
}

//#define VIDEOFILESDIRECTORY "/home/shoaib/SmartDoc/smartdoc/samples"
//#define IMAGEFILESDIRECTORY "/home/shoaib/SmartDoc/smartdoc/samples"

/** 
 * This function is responsible for reading a video file
 * and detecting documents in each of the frames by delegating this task to Detect function
 */ 
void SmartDoc::readVideoFiles(string path)
{
 	vector<string> files;
 	getFilesFromDirectory(path, files, VIDEOFILESFORMAT);

 	if(files.size() == 0)
 	{
 		cerr << "Error: Unable to find any files" << endl;
 		exit(-1);
 	}

	//Iterate over all the files
	for(int i = 0; i < files.size(); i++)
	{
		cout << "Starting video: " << files[i] << endl;

		//Open the video file for reading
		VideoCapture videoCapture(files[i]);

		if(!videoCapture.isOpened())
		{
			cerr << "Error: Unable to open video file" << endl;
			exit(-1);
		}

		//Extract the video file name from the absolute path
		videoFileName = files[i].substr(files[i].find_last_of("/") + 1);
		videoFileName = videoFileName.substr(0, videoFileName.find_last_of("."));
		videoFileName += ".xml";
		videoFileName = OUTPUTXMLFILEDIRECTORY + videoFileName;
		cout << videoFileName << endl;

		//Create basic structure in the file
		initializeXMLFile();

		//Start reading the video frames
		int frameCount = videoCapture.get(CAP_PROP_FRAME_COUNT);
		cout << "Frame Count: " << frameCount << endl;
		Mat frame;

		//Iterate over all the frames in the video
		for(int frameNo = 0; frameNo < frameCount; frameNo++)
		{
		    if (frameNo > 15) break;
			//Capture frame
			videoCapture >> frame;

			if(frame.empty())
			{
				cerr << "Error: Empty frame found" << endl;
				break;
			}
            if(frameNo%3) continue;
			//Send the frame for detection
			Mat processedFrame;
			processFrame(frame, processedFrame, (frameNo + 1));
		}

		//Write the XML document to file
		document.print(cout);
    	document.save_file(videoFileName.c_str());
	}
}

/** 
 * This function is responsible for reading in all the image files from the directory
 * and detecting documents in each of the frames by delegating this task to Detect function.
 * Once the document is detected, the frame is passed on to the recoginize text function 
 * which performs text detection tasks using Tesseract API
 */ 
void SmartDoc::readImageFiles(string path)
{
 	vector<string> files;
 	getFilesFromDirectory(path, files, IMAGEFILESFORMAT);

 	if(files.size() == 0)
 	{
 		cerr << "Error: Unable to find any files" << endl;
 		exit(-1);
 	}

	//Iterate over all the files
	for(int i = 0; i < files.size(); i++)
	{
		//Read the image from file
		Mat image = imread(files[i]);

		if(image.empty())
		{
			cerr << "Error: Unable to load image" << endl;
			break;
		}

		//Send the frame for detection
		Mat processedFrame;
		//resize(image, image, Size(), 0.25, 0.25, INTER_AREA);
		processFrame(image, processedFrame, 0);

		//Extract the image file name from the absolute path
		string::size_type pAtPath = files[i].find_last_of('/');
		string filePath = files[i].substr(0,pAtPath+1); 
		string fileFullName = files[i].substr(pAtPath+1);
		string::size_type pAtExt = fileFullName.find_last_of('.');
		string fileBaseName = fileFullName.substr(0,pAtExt);
		
		string outFileName = filePath + fileBaseName + "-doc.png";
		imwrite(outFileName, processedFrame);
		cout << "Writing " << outFileName << endl;

		//Apply text detection
		recognizeText(processedFrame, files[i]);
	}
}


Rect getBounds(Mat imgBin){

int documentLeft, documentTop, documentRight, documentBottom; 
    double borderThresh = 0.8; 
    for(int i=0; i<imgBin.rows; i ++){ 
        double blackPixels = 0; 
        for(int j=0; j<imgBin.cols; j++){ 
            if (imgBin.at<uchar>(i,j) == 0) 
                blackPixels ++; 
        } 
        if( (blackPixels / imgBin.cols) < borderThresh){ // top edge found 
            documentTop = i; 
            break; 
        } 
    } 
 
    for(int i=imgBin.rows-1; i>=0; i--){ 
        double blackPixels = 0; 
        for(int j=0; j<imgBin.cols; j++){ 
            if (imgBin.at<uchar>(i,j) == 0) 
                blackPixels ++; 
        } 
        if( (blackPixels / imgBin.cols) < borderThresh){ // top edge found 
            documentBottom = i; 
            break; 
        } 
    } 
 
    for(int j=0; j<imgBin.cols; j++){ 
        double blackPixels = 0; 
        for(int i=0; i<imgBin.rows; i ++){ 
            if (imgBin.at<uchar>(i,j) == 0) 
                blackPixels ++; 
        } 
        if( (blackPixels / imgBin.rows) < borderThresh){ // top edge found 
            documentLeft = j; 
            break; 
        } 
    } 
 
    for(int j=imgBin.cols-1; j>=0; j--){ 
        double blackPixels = 0; 
        for(int i=0; i<imgBin.rows; i ++){ 
            if (imgBin.at<uchar>(i,j) == 0) 
                blackPixels ++; 
        } 
        if( (blackPixels / imgBin.rows) < borderThresh){ // top edge found 
            documentRight = j; 
            break; 
        } 
    }
	Rect document(documentLeft, documentTop, documentRight - documentLeft, documentBottom - documentTop); 
	return document;
}

void SmartDoc::processFrame(Mat &frame, cv::Mat &processedFrame, int index)
{
	cout<<"-----------------------"<<endl;
    // Clip card region 
    Mat grayscaleImg;
    cvtColor(frame, grayscaleImg, COLOR_BGR2GRAY);

    //Custom Cropping (Dr. Faisal)
    Mat imgBin; 
    binarizeOtsu(grayscaleImg, imgBin);
 	
  	Rect document= getBounds(imgBin);
//   	processedFrame = frame(document);

    //Save the bounding box in an XML file for Challenge 1
    if(CHALLENGE == 1)
    {
    	//Write bounding box to file
    	appendDocumentCoordinatesInXMLFile(document, index);
    } 

    //imshow("Input Frame", grayscaleImg);

    Mat cannyimg=GetCannyImage(grayscaleImg,150,3,5);
	Mat prob_houghLines=Probabilistic_houghLines(cannyimg,5,100,100);
	//Mat Std_houghLines1=Std_houghLines(cannyimg,5);
    //Mat lSDOutput=lineSegmentDectector(grayscaleImg); 
    //Mat Countour=Getcontours(cannyimg);
     
    
    float max_x=0.9; 
    float max_y=0.9;
    float min_area=100;
    int type=8;
    std::vector<cv::Rect> rboxes;
    findConComp(prob_houghLines, rboxes, max_x, max_y, min_area, type);

//    Mat comps = frame.clone();
    
//    Mat comps;
//    vector<Mat> channels;
//    Mat red = prob_houghLines.clone();
//    Mat green = prob_houghLines.clone();
//    Mat blue = prob_houghLines.clone();
//    channels.push_back(blue);
//    channels.push_back(green);
//    channels.push_back(red);
//    merge(channels, comps);
    
    Mat comps;
    vector<Mat> channels;
    Mat red = grayscaleImg.clone();
    Mat green = grayscaleImg.clone();
    Mat blue = grayscaleImg.clone();
    channels.push_back(blue);
    channels.push_back(green);
    channels.push_back(red);
    merge(channels, comps);
    
    double max_area = 0.0;
    Rect doc;
    
    for(int i=0; i< rboxes.size(); i++){
        double a = rboxes[i].width * rboxes[i].height;
        if(a > max_area){
            doc = rboxes[i];
            max_area = a;
        }
    }

    rectangle(comps, doc, Scalar(0,0,255), 3);
                 
//    imshow("Output Frame", comps);
    processedFrame = frame(doc);

//    char c = waitKey(100);
//    if(c == 'q'){
//        exit(0);
//    }
    return;
}



/** 
 * This function implements global image thresholding using Otsu's algorithm 
 *  
 * @param[in]     gray Gray scale input image as an OpenCV Mat object. 
 * @param[out]    binary Output image binarized using Otsu's method. 
 */ 
void SmartDoc::binarizeOtsu(Mat &gray, Mat &binary)
{ 
	//Apply Guassian Blur (3x3)
	//blur( gray, gray, Size(3,3) );

	//Opening
	//erode(gray, gray, Mat());
    //dilate(gray, gray, Mat());

	//Closing
    //dilate(gray, gray, Mat());
    //erode(gray, gray, Mat());

	//Threshold = 0; Max_Val = 255
    threshold(gray, binary, 0, 255, THRESH_BINARY | THRESH_OTSU); 

}

/** 
 * This function implements local adaptive thresholding algorithms from: 
 * Faisal Shafait, Daniel Keysers, Thomas M. Breuel. "Efficient Implementation  
 * of Local Adaptive Thresholding Techniques Using Integral Images",  
 * SPIE Document Recognition and Retrieval XV, DRR'08, San Jose, CA, USA. Jan. 2008 
 *  
 * @param[in]     gray Gray scale input image as an OpenCV Mat object. 
 * @param[out]    binary Output image binarized using Shafait's method. 
 * @param[in]     w local square window side length to compute adaptive threshold. 
 * @param[in]     k Gray level sensititivity parameter. Lower values of k result in whiter images (fewer black pixels) and vice versa. 
 */ 
void SmartDoc::binarizeShafait(Mat &gray, Mat &binary, int w, double k)
{ 
    Mat sum, sumsq; 
    gray.copyTo(binary); 
    int half_width = w >> 1; 
    integral(gray, sum, sumsq,  CV_64F); 
    for(int i=0; i<gray.rows; i ++){ 
        for(int j=0; j<gray.cols; j++){ 
            int x_0 = (i > half_width) ? i - half_width : 0; 
            int y_0 = (j > half_width) ? j - half_width : 0; 
            int x_1 = (i + half_width >= gray.rows) ? gray.rows - 1 : i + half_width; 
            int y_1 = (j + half_width >= gray.cols) ? gray.cols - 1 : j + half_width; 
            double area = (x_1-x_0) * (y_1-y_0); 
            double mean = (sum.at<double>(x_0,y_0) + sum.at<double>(x_1,y_1) - sum.at<double>(x_0,y_1) - sum.at<double>(x_1,y_0)) / area; 
            double sq_mean = (sumsq.at<double>(x_0,y_0) + sumsq.at<double>(x_1,y_1) - sumsq.at<double>(x_0,y_1) - sumsq.at<double>(x_1,y_0)) / area; 
            double stdev = sqrt(sq_mean - (mean * mean)); 
            double threshold = mean * (1 + k * ((stdev / 128) -1) ); 
            if (gray.at<uchar>(i,j) > threshold) 
                binary.at<uchar>(i,j) = 255; 
            else 
                binary.at<uchar>(i,j) = 0; 
        } 
    } 
}

/** 
 * This function implements directory traversal using boost filesystem libraries
 *  
 * @param[in]     Name of the directory to be traversed 
 * @param[in]     Format of the files to be searched
 * @param[out]    Vector containing absolute paths of all the files
 */ 
void SmartDoc::getFilesFromDirectory(string dir, vector<string> &files, string format)
{
	path current_dir(dir);

	for (recursive_directory_iterator iter(current_dir), end; iter != end; ++iter)
	{
		string name = iter->path().filename().string();
		//cout << iter->path() << endl;
		if (boost::ends_with(name, format))
		{
			files.push_back(iter->path().string());
		}
	}
}

/** 
 * This function implements text recognition using Tesseract API
 *  
 * @param[in]     Image file for text recognition
 * @param[out]    Stores all the words recognized in an XML file
 */ 
bool SmartDoc::recognizeText(Mat& documentImage, string filename)
{
    string::size_type pAtPath = filename.find_last_of('/');
    string filePath = filename.substr(0,pAtPath+1); 
    string fileFullName = filename.substr(pAtPath+1);
    string::size_type pAtExt = fileFullName.find_last_of('.');
    string fileBaseName = fileFullName.substr(0,pAtExt);
    
	// Rescale image to have the larger dimension (width) to m_LargerDim
    // to improve Tesseract's result
 	// int maxDim = max(documentImage.cols, documentImage.rows); 
 	// Mat rescaledImg; 
	// double scale = m_LargerDim / maxDim;
	// resize(imgMATColor, rescaledImg, Size(), scale, scale, INTER_LANCZOS4);
    
    // Binarize the original image again using local thresholding
    // Improves some cases, deteriorates others
    Mat imgMATgray, imgMATbinSh; 
    cvtColor(documentImage, imgMATgray, COLOR_RGB2GRAY);
    binarizeShafait(imgMATgray, imgMATbinSh, 30, 0.05);

    if(false){
        string binImgFileName = filePath + fileBaseName + "-01-bin-Shafait.png";
        imwrite(binImgFileName.c_str(), imgMATbinSh);
    }

    // Convert image to Tesseract format
    Pix *imgPIX = MAT2PIXBinary(imgMATbinSh);
    
    double tcStartTess = (double)getTickCount(); 
    tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI(); 
    // Initialize tesseract-ocr with English, without specifying tessdata path 
    if (api->Init(NULL, "eng")) { 
        fprintf(stderr, "Could not initialize Tesseract OCR.\n"); 
        return false; 
    }
    
    // Feed pre-thresholded image to Tesseract
    api->SetImage(imgPIX);
    
   
//    api->SetPageSegMode(PSM_OSD_ONLY);
//    OSResults osresult;
//    api->DetectOS(&osresult);

//    // Write Outputs
//    double conf = osresult.best_result.oconfidence;
//    int orientation_id = osresult.best_result.orientation_id;
//    fprintf(stderr, "Estimated orientation = %d degrees\n", 90 * orientation_id);
//    fprintf(stderr, "Orientation estimation confidence = %.2f\n", conf);
//    

    char *outText; 

    // Tesseract First Run to get card layout
    api->SetPageSegMode(PSM_AUTO_OSD);
    api->SetVariable("tessedit_char_whitelist", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.@-:()&_+,");
 
    //Output stream for saving detected text in a file
    ofstream outputStream;
    string outputTextFile = filePath + fileBaseName + "-ocr.txt";
    outputStream.open(outputTextFile.c_str());

    outText = api->GetUTF8Text();
    outputStream << outText;

    // Destroy used object and release memory
    api->End();
    delete [] outText;
    pixDestroy(&imgPIX);

    //Close the output stream
    outputStream.close();
}

/** 
 * This function initializes the XML file by writing its basic structure
 *  
 * @param[in]     None
 * @param[out]    None
 */ 
void SmartDoc::initializeXMLFile()
{
	document.reset();
	//<?xml version='1.0' encoding='utf-8'?>

	//<ground_truth version="0.2" generated="2014-11-27T20:36:46.513598">
    //Add parent node
    xml_node ground_truth = document.append_child();
    ground_truth.set_name("ground_truth");
    ground_truth.append_attribute("version") = "0.2";
    string dateAndTime = currentDateTime();
    //cout << dateAndTime << endl;
    ground_truth.append_attribute("generated") = dateAndTime.c_str();

	//<software_used name="GT builder with inpainting" version="0.4"/>
    xml_node software_used = ground_truth.append_child();
    software_used.set_name("software_used");
    software_used.append_attribute("name") = "SmartDoc";
    software_used.append_attribute("version") = "0.1";

	//<source_sample_file>background00/datasheet001.mp4</source_sample_file>
    xml_node source_sample_file = ground_truth.append_child();
    source_sample_file.set_name("source_sample_file");
    source_sample_file.set_value("checking");
    /*
    if(!source_sample_file.set_value(videoFileName.c_str()))
    {
    	cerr << "Error occured while writing" << endl;
    }*/

	//<segmentation_results>
	segmentation_results = ground_truth.append_child();
	segmentation_results.set_name("segmentation_results");
}

/** 
 * This function returns the current date and time
 *  
 * @param[in]     None
 * @param[out]    string containing the data and time in the specified format
 */ 
string SmartDoc::currentDateTime() 
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%dT%X", &tstruct);

    return buf;
}

/** 
 * This function writes the Rect coordinates provided to it in the XML file
 *  
 * @param[in]     Rect whose coordinates needs to be saved in the XML file
 * @param[out]    None
 */ 
void SmartDoc::appendDocumentCoordinatesInXMLFile(Rect document, int index)
{
	//<frame index="1" rejected="false">
	xml_node frame = segmentation_results.append_child();
	frame.set_name("frame");
	frame.append_attribute("index") = index;

	//Add coordinates
	//<point name="bl" x="716.625" y="805.609"/>
	xml_node point = frame.append_child();
	point.set_name("point");
	point.append_attribute("name") = "tl";
	point.append_attribute("x") = document.tl().x;
	point.append_attribute("y") = document.tl().y;

	point = frame.append_child();
	point.set_name("point");
	point.append_attribute("name") = "br";
	point.append_attribute("x") = document.br().x;
	point.append_attribute("y") = document.br().y;

	point = frame.append_child();
	point.set_name("point");
	point.append_attribute("name") = "bl";
	point.append_attribute("x") = document.tl().x;
	point.append_attribute("y") = document.br().y;

	point = frame.append_child();
	point.set_name("point");
	point.append_attribute("name") = "tr";
	point.append_attribute("x") = document.br().x;
	point.append_attribute("y") = document.tl().y;
}



//Main Method
int main(int argc, char **argv)
{
    if(argc < 2){
        cerr << "Usage: " << argv[0] << " <path>\n";
        exit(-1);
    }
	SmartDoc smartDoc;

	if(CHALLENGE == 1)
	{
		smartDoc.readVideoFiles(argv[1]);
	}
	else
	{
		smartDoc.readImageFiles(argv[1]);
	}
}
