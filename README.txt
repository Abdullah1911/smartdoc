/////////////////////////////////////////////////////////
					Smart Doc
/////////////////////////////////////////////////////////

The following repository contains the source code for Smart Doc Competition 2015.

The code base contains a main file called as smartDoc.cc which contains all the code necessary. The code is compiled using scons command from the src folder which is an automated build tool. The program can then be executed by calling the smartDoc executable e.g. ./smartDoc.

Smart Doc searches for video files if challenge 1 is selected and for image files if challenge 2 is selected from smartDoc.h.

Once files are searched, they are processed. Video are processed frame by frame while images are processed directly. After the processing is finished, challenge 2 is to perform OCR on the documents, therefore for the second task, images are fed into the recognizeText function which recognizes the text written on it. For the first task, the output is an XML file containing the bounding boxes of all the detected documents. Therefore, an XML library called as PugiXML is used for this purpose. For the second task, the output is a plain text file containing the detected text. Therefore, a simple text file is written using in-built c++ methods.


//////////////////////////////////////////////////////////
Dated: 18th March 2015
Author: Shoaib Ahmed Siddiqui